# @Author: Spencer Hallyburton <spencer>
# @Date:   2021-07-02
# @Filename: test_aws.py
# @Last modified by:   Spencer
# @Last modified time: 2021-07-05T21:54:05-04:00

"""
Taken from: https://github.com/aws-samples/aws-python-sample/blob/master/s3_sample.py
"""

import os
# import shutil
import uuid
import logging
import boto3
from botocore.exceptions import ClientError


def get_bucket(bucket_name):
    """returns the S3 bucket"""
    s3resource = boto3.resource('s3')
    return s3resource.Bucket(bucket_name)


def file_exists(bucket_name, object_name):
    s3_client = boto3.client('s3')
    try:
        s3_client.head_object(Bucket=bucket_name, Key=object_name)
    except ClientError as e:
        if e.response['Error']['Code'] == "404":
            return False
        else:
            raise
    else:
        return True


def get_files_in_folder():
    pass


def bucket_exists(bucket_name):
    """Check if the bucket exists"""
    s3_client = boto3.client('s3')
    list_buckets_resp = s3_client.list_buckets()
    for bucket in list_buckets_resp['Buckets']:
        if bucket['Name'] == bucket_name:
            return True
    return False


def delete_bucket(bucket_name, delete_in_bucket=False):
    """Deletes the bucket"""
    bucket = get_bucket(bucket_name)
    if delete_in_bucket:
        bucket.objects.delete()
    bucket.delete()


def create_bucket(bucket_name):
    """Create an AWS S3 Bucket"""
    s3client = boto3.client('s3')
    try:
        s3client.create_bucket(Bucket=bucket_name)
    except ClientError as e:
        logging.error(e)
        return False
    return True


def upload_folder(folder_name, bucket):
    """Upload contents of a folder to AWS

    :param folder_name: path to the folder to be uploaded
    :param bucket: bucket to upload to
    """
    for (root,dirs,files) in os.walk(folder_name, topdown=True):
        for name in files:
            full_path = os.path.join(root, name)
            upload_file(full_path, bucket)


def download_all_prefix(prefix, bucket_name, save_folder='.', strip_path=False):
    """Download all files with a certain prefix on AWS

    :param prefix: the prefix of the downloads
    :param bucket: the name of the bucket to access
    """
    s3_resource = boto3.resource('s3')
    bucket = s3_resource.Bucket(bucket_name)
    for obj in bucket.objects.filter(Prefix = prefix):
        if not os.path.exists(os.path.dirname(obj.key)):
            os.makedirs(os.path.dirname(obj.key))
        if strip_path:
            file_name = obj.key.split('/')[-1]
        else:
            file_name = obj.key
        download_file(obj.key, bucket_name, os.path.join(save_folder, file_name))


def download_folder(folder_name, bucket):
    """Download contents from a "folder" on AWS

    :param folder_name: prefix (folder) for the items to be downloaded
    :param bucket: bucket to downlod from
    """
    download_all_prefix(folder_name, bucket, folder_name, strip_path=True)
    return True


def upload_file(file_name, bucket, object_name=None):
    """Upload a file to an S3 bucket

    :param file_name: File to upload
    :param bucket: Bucket to upload to
    :param object_name: S3 object name. If not specified then file_name is used
    :return: True if file was uploaded, else False
    """

    # If S3 object_name was not specified, use file_name
    if object_name is None:
        object_name = file_name

    # Upload the file
    s3_client = boto3.client('s3')
    try:
        response = s3_client.upload_file(file_name, bucket, object_name)
    except ClientError as e:
        logging.error(e)
        return False
    return True


def download_file(object_name, bucket, file_name=None):
    """Download a file from an S3 bucket

    :param object_name: S3 object name
    :param bucket: Bucket to download from
    :param file_name: name of file to save result to locally. If not specified, try to use object_name

    """
    if file_name is None:
        file_name = object_name

    s3_client = boto3.client('s3')
    try:
        s3_client.download_file(bucket, object_name, file_name)
    except ClientError as e:
        logging.error(e)
        return False
    return True
