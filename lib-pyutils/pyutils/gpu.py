# @Author: Spencer Hallyburton <spencer>
# @Date:   2021-09-03
# @Filename: gpu.py
# @Last modified by:   spencer
# @Last modified time: 2021-09-03


import subprocess as sp
import os
import numpy as np
import time


def get_gpu_memory(units='MB'):
  _output_to_list = lambda x: x.decode('ascii').split('\n')[:-1]

  ACCEPTABLE_AVAILABLE_MEMORY = 1024
  COMMAND = "nvidia-smi --query-gpu=memory.free --format=csv"
  memory_free_info = _output_to_list(sp.check_output(COMMAND.split()))[1:]
  memory_free_values = [int(x.split()[0]) for i, x in enumerate(memory_free_info)]

  # Convert units
  if units.lower() == 'mb':
      pass
  elif units.lower() == 'gb':
      memory_free_values = [mem/1e3 for mem in memory_free_values]

  return memory_free_values


def wait_on_gpu_memory(t_wait_max=300, mem_req=1, units='GB', delay_time=5):

    # Wait on gpu memory to be available
    t_elapsed = 0
    while True:
        # Check if there's memory
        gpu_free = np.any(np.asarray(get_gpu_memory(units=units)) > mem_req)
        if gpu_free:
            print('GPU memory is available!')
            break
        else:
            if t_elapsed > t_wait_max:
                raise RuntimeError('Took too long waiting on GPU memory')
            else:
                print('Still waitng on gpu memory...%d seconds elapsed...\r'%t_elapsed, end="")
                time.sleep(delay_time)
                t_elapsed += delay_time
    return


if __name__ == "__main__":
    gpu_mem = get_gpu_memory()
    print(gpu_mem)
