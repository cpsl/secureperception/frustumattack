# -*- coding: utf-8 -*-
# @Author: Spencer H
# @Date:   2021-01-13
# @Last modified by:   spencer
# @Last Modified date: 2021-04-02
# @Description:
"""
Performs intelligent, adaptive, and customizable alerts to the user.
"""


def read_mail_key_file(key_path):
    """Read in the file mail_key.txt by providing the path to it"""
    with open(key_path, 'r') as fp:
        email_p = fp.read()
    pwds = {}
    for item in email_p.split('\n'):
        idx = item.find(':')
        if idx == -1:
            continue
        key = item[0:idx]
        value = item[idx+2::]
        pwds[key] = value
    return pwds


def get_computer():
    """Gets computer hostname"""
    import socket
    hn = socket.gethostname()
    if hn in ['nirvana', 'megadeth', 'hancock', 'wilco']:
        if hn in ['nirvana', 'megadeth']:
            kp = '/home/spencer/Private/mail_key.txt'
        elif hn in ['hancock', 'wilco']:
            kp = '/home/pi/Private/mail_key.txt'
        else:
            print(hn)
            RuntimeError('Cannot find key path')
    else:
        print(hn)
        RuntimeError('Cannot understand hostname')
    return hn, kp


def send_alert(send_to='spencer.hallyburton@duke.edu',
               subject='Test alert with no content',
               message='No content',
               debug=False):
    """ Sends an email alert when queried

    Reads in from file to determine which email to send from.

    Keyword inputs:
    send_to -- email address to send the alert to
    subject -- subject of the email
    message -- email message body

    Exceptions handled:
    FileNotFoundError -- if cannot find passwords file, alerts user
    PermissionError -- if does not have permissions, alerts user
    All others -- it alerts user
    """
    if debug:
        print('Entering send_alert function')

    try:
        import smtplib
        import ssl
        from email.mime.text import MIMEText

        if debug:
            print('Successfully imported modules')

        # Account login information
        hostname, key_path = get_computer()
        if debug:
            print('Hostname is \"{}\"'.format(hostname))
        email_user = '{}@shally.dev'.format(hostname)

        # Get pwd
        if debug:
            print('Looking in file {}'.format(key_path))
        pwds = read_mail_key_file(key_path)
        email_p = pwds[hostname]

        # Set up server
        if debug:
            print('Attempting to connect to SMTP server...')
        context = ssl.create_default_context()
        server = smtplib.SMTP_SSL(host='box2034.bluehost.com',
                                  port=465, context=context)
        if debug:
            print('Connection passed!')
    #     server.set_debuglevel(1)
    #     server.starttls()
        if debug:
            print('Attempting to log in to email...')
        server.ehlo()
        server.login(email_user, email_p)
        if debug:
            print('Login passed!')

        # To and from
        sent_from = '{} <{}>'.format(hostname, email_user)

        # Fill in message
        msg = MIMEText(message)
        msg['Subject'] = subject
        msg['From'] = sent_from
        msg['To'] = send_to

        # Send mail
        if debug:
            print('Attempting to send email')
        server.sendmail(sent_from, send_to, msg.as_string())
        server.quit()
        print('Email sent!')
        return
    except FileNotFoundError:
        print('Cannot find file...')
    except PermissionError:
        print('No permissions to access file...')
    print('Continuing without sending alert')
