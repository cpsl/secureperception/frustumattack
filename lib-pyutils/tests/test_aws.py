# @Author: Spencer Hallyburton <spencer>
# @Date:   2021-07-04
# @Filename: test_aws.py
# @Last modified by:   Spencer
# @Last modified time: 2021-07-05T21:53:49-04:00

import os
import shutil
import pyutils.aws as aws


test_bucket_name = 'test-bucket-roshambo919'


def test_create_and_delete_bucket():
    bucket_name = test_bucket_name
    if aws.bucket_exists(bucket_name):
        aws.delete_bucket(bucket_name, True)
    aws.create_bucket(bucket_name)
    assert(aws.bucket_exists(bucket_name))
    aws.delete_bucket(bucket_name)
    assert(not aws.bucket_exists(bucket_name))


def test_upload_and_download():
    bucket_name = test_bucket_name
    if aws.bucket_exists(bucket_name):
        aws.delete_bucket(bucket_name, True)
    aws.create_bucket(bucket_name)

    # Create at test file
    test_text = 'test_file!\nthis is a test'
    file_name = 'test.txt'
    with open(file_name, 'w') as f:
        f.write(test_text)

    # Upload, check if exists, download, check result
    assert(aws.upload_file(file_name, bucket_name))
    assert(aws.file_exists(bucket_name, file_name))
    assert(aws.download_file(file_name, bucket_name))
    with open(file_name, 'r') as f:
        assert(f.read() == test_text)


def test_upload_and_download_folder():
    bucket_name = test_bucket_name
    if aws.bucket_exists(bucket_name):
        aws.delete_bucket(bucket_name, True)
    aws.create_bucket(bucket_name)

    # Create test files
    dirname = 'test_dir'
    if os.path.exists(dirname):
        shutil.rmtree(dirname)
    os.mkdir(dirname)
    test_text = ['test_file1', 'test_file2']
    for i, txt in enumerate(test_text):
        file_name = os.path.join(dirname, 'test_file%i.txt'%i)
        with open(file_name, 'w') as f:
            f.write(txt)

    # Upload
    aws.upload_folder(dirname, bucket_name)

    # Remove files locally
    shutil.rmtree(dirname)

    # Test existence
    for i in range(len(test_text)):
        file_name = os.path.join(dirname, 'test_file%i.txt'%i)
        assert(aws.file_exists(bucket_name, file_name))

    # Download
    assert(aws.download_folder(dirname, bucket_name))

    # Test files are here
    assert(os.path.exists(dirname))
    for i in range(len(test_text)):
        assert(os.path.exists(os.path.join(dirname, 'test_file%i.txt'%i)))

    # Remove at end
    shutil.rmtree(dirname)
    aws.delete_bucket(bucket_name, True)
