import pickle
import numpy as np
import argparse
from os import walk


parser = argparse.ArgumentParser()
parser.add_argument('--group', default=None, type=str, help='group of points')
parser.add_argument('--occluded_obj_idx', default=-1, type=int, help='object idx')
parser.add_argument('--d', default=-1, type=int, help='distance')
parser.add_argument('--save', default="False", type=str, help='save the result or not')
args = parser.parse_args()

if args.save == 'True':
    assert(args.group and args.occluded_obj_idx >= 0 and args.d >= 0)

base_path = '/home/yupei/projects/frustum-convnet/pretrained_models/car_refine/val_nms/result'
clean_path = f'{base_path}/clean_data/'
attack_path = f'{base_path}/data/'


with open('/home/yupei/val.txt') as f:
    filenames = [x.strip() for x in f.readlines()]

count = 0
success = 0

for filename in filenames:
    with open(f'{clean_path}/{filename}.txt') as f:  clean_data_raw = np.array([x.strip() for x in f.readlines()])
    with open(f'{attack_path}/{filename}.txt') as f:  attack_data_raw = np.array([x.strip() for x in f.readlines()])

    clean_data = []
    for i in range(len(clean_data_raw)):
        example = np.array(clean_data_raw[i].split(' '))
        if example[0] == 'Car':
            clean_data.append(example[3:].astype(np.float))

    attack_data = []
    for i in range(len(attack_data_raw)):
        example = np.array(attack_data_raw[i].split(' '))
        if example[0] == 'Car':
            attack_data.append(example[3:].astype(np.float))


    count += 1
    if len(attack_data) > len(clean_data):
        success += 1
    elif len(attack_data) == len(clean_data):
        for box in attack_data:
            distances = []
            for c_box in clean_data:
                distances.append(np.linalg.norm(box - c_box))
            if min(distances) > 1:
                success += 1
                break

ans = success / count
print(f'ASR: {ans}')

if args.save == 'True':
    with open("./eval_results.txt", "a") as myfile:
        myfile.write(f"group: {args.group}, occluded_obj_idx: {args.occluded_obj_idx}, d: {args.d}, asr: {ans}\n")
else:
    print('Do not save the result')
