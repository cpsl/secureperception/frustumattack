# @Author: Spencer Hallyburton <spencer>
# @Date:   2021-06-04
# @Filename: eval_custom_experiment.sh
# @Last modified by:   spencer
# @Last modified time: 2021-07-08

#!/bin/bash
# set -x
# set -e

# Inputs:
# 1 - model path, e.g.: pretrained_models/car/model_0050.pth
# 2 - output,     e.g.: /full/path/to/detection_results_v1
# 3 - data path,  e.g.: fcn-experiment-test
# 4 - do_refine,  e.g.: true

ARG1=${1:-pretrained_models/car/model_0050.pth}
ARG2=${2:-./results/fcn-experiment-test/}
ARG3=${3:-fcn-experiment-test}
ARG4=${4:-true}

export CUDA_VISIBLE_DEVICES=0
MODELPATH="$ARG1"
OUTDIR="$ARG2"
DATADIR="$OUTDIR/data"

# Activate conda environment
conda activate fcn

echo "Entering testing"
python train/test_net_det.py --cfg cfgs/det_sample_custom.yaml OUTPUT_DIR $OUTDIR TEST.DATASET $ARG3 TEST.WEIGHTS $MODELPATH

if "$ARG4"; then
  echo "Entering data refinement"
  python kitti/prepare_data_refine.py --car_only --output_custom $OUTDIR

  REFINE_STR="_refine"
  OUTDIR_REFINE="$OUTDIR$REFINE_STR"

  TESTFILE="$OUTDIR/frustum_caronly_rgb_detection_refine.pickle"
  echo "Entering refined testing"
  python train/test_net_det.py --cfg cfgs/refine_car_custom.yaml OUTPUT_DIR $OUTDIR_REFINE TEST.WEIGHTS $MODELPATH \
                               OVER_WRITE_TEST_FILE $OUTDIR
else
  echo "Skipping refine"
fi

# Deactivate conda environment
conda deactivate
