# @Author: Spencer Hallyburton <spencer>
# @Date:   2021-06-05
# @Filename: prepare_custom_data.sh
# @Last modified by:   spencer
# @Last modified time: 2021-07-08

# Inputs:
# 1 - data path,  e.g.: fcn-experiment-test

ARG1=${1:-fcn-experiment-test}
echo $ARG1

conda activate fcn
rc=$?; if [[ $rc != 0 ]]; then exit $rc; fi

python kitti/prepare_data.py --car_only --path_custom "$ARG1"
rc=$?; if [[ $rc != 0 ]]; then exit $rc; fi

conda deactivate
rc=$?; if [[ $rc != 0 ]]; then exit $rc; fi
