# @Author: Spencer Hallyburton <spencer>
# @Date:   2021-07-23
# @Filename: car_train_from_monodepth.sh
# @Last modified by:   spencer
# @Last modified time: 2021-07-23


#!/bin/bash
set -x
set -e

export CUDA_VISIBLE_DEVICES=0
REPREP=false


# Prepare data
TRAINDATA="training_experiment_from_mono"
VALDATA="validation_experiment_from_mono"
if [ "$REPREP" = true ]
then
  echo "Preparing training data"
  python kitti/prepare_data.py --car_only --path_custom $TRAINDATA

  echo "Preparing validation data"
  python kitti/prepare_data.py --car_only --path_custom $VALDATA
fi


# Training
OUTDIR='output/car_train_from_mono'
echo "Entering training from monodepth"
python train/train_net_det.py --cfg cfgs/det_sample_custom.yaml OUTPUT_DIR $OUTDIR TRAIN.DATASET $TRAINDATA TEST.DATASET $VALDATA

# Evaluation
MODEL="$OUTDIR/model_0050.pth"
echo "Evaluating training from monodepth"
python train/test_net_det.py --cfg cfgs/det_sample_custom.yaml OUTPUT_DIR $OUTDIR TEST.DATASET $VALDATA TEST.WEIGHTS $MODEL

# Refinement??
