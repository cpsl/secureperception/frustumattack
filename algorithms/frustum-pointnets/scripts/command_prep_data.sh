# @Author: Spencer Hallyburton <spencer>
# @Date:   2021-03-09
# @Filename: command_prep_data.sh
# @Last modified by:   spencer
# @Last modified time: 2021-07-21


#/bin/bash
python kitti/prepare_data.py --gen_train --gen_val --gen_val_rgb_detection
