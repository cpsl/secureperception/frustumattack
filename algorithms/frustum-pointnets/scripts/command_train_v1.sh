# @Author: Spencer Hallyburton <spencer>
# @Date:   2021-07-21
# @Filename: command_train_v1.sh
# @Last modified by:   spencer
# @Last modified time: 2021-07-21


#/bin/bash
python train/train.py --gpu 0 --model frustum_pointnets_v1 --log_dir train/log_v1 --num_point 2048 --max_epoch 100 --batch_size 32 --decay_step 400000 --decay_rate 0.7 --restore_model_path train/log_v1/model.ckpt
