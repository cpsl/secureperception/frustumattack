# @Author: Spencer Hallyburton <spencer>
# @Date:   2021-04-22
# @Filename: command_prep_custom_data.sh
# @Last modified by:   spencer
# @Last modified time: 2021-07-13


#/bin/bash

# Inputs:
# 1 - data path, e.g.: fpn-experiment-2
# 2 - type override string

ARG1=${1:-fpn-experiment-2}
ARG2=${2}
echo $ARG1

# Activate conda environment
conda activate fpn

if [ "$#" -eq 1 ]; then
  python kitti/prepare_data.py --path_custom "$ARG1"
elif [ "$#" -eq 2 ]; then
  python kitti/prepare_data.py --path_custom "$ARG1" --type_override "$ARG2"
fi

# Deactivate conda environment
conda deactivate
