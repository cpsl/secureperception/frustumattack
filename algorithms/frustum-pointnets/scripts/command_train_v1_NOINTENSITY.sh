#/bin/bash
python train/train.py --gpu 1 --model frustum_pointnets_v1 --log_dir train/log_v1_NOINTENSITY --num_point 2048 --max_epoch 100 --batch_size 32 --decay_step 400000 --decay_rate 0.7 --no_intensity
