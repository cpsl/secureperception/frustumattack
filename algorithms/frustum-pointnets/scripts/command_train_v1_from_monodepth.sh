# @Author: Spencer Hallyburton <spencer>
# @Date:   2021-07-21
# @Filename: command_train_v1_from_monodepth.sh
# @Last modified by:   spencer
# @Last modified time: 2021-07-22


#/bin/bash


# Prep data
REPREP=false

if [ "$REPREP" = true ]
then
  echo "Preparing training data"
  python kitti/prepare_data.py --path_custom "training_experiment_from_mono"

  echo "Preparing validation data"
  python kitti/prepare_data.py --path_custom "validation_experiment_from_mono"
fi

# Training
echo "Entering training from monodepth"
python train/train.py --from_mono --gpu 0 --model frustum_pointnets_v1 --log_dir train/log_v1_from_mono --num_point 2048 --max_epoch 100 --batch_size 32 --decay_step 400000 --decay_rate 0.7 --restore_model_path train/log_v1_from_mono/model.ckpt
