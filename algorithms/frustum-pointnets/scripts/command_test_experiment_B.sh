# @Author: Spencer Hallyburton <spencer>
# @Date:   2021-03-15
# @Filename: command_test_experiment_B.sh
# @Last modified by:   spencer
# @Last modified time: 2021-07-13


#/bin/bash

# Inputs:
# 1 - model path, e.g.: log_v1
# 2 - output,     e.g.: /full/path/to/detection_results_v1
# 3 - data path,  e.g.: fpn-experiment-2
# 4 - (optional) intensity - true means engage, false not

ARG1=${1:-log_v1}
ARG2=${2:-detection_results_v1}
ARG3=${3:-fpn-experiment-2}
ARG4=${4:-true}

# define variables
model_path="train/$ARG1/model.ckpt"
output="$ARG2"
data_path="kitti/frustum_carpedcyc_$ARG3.pickle"
idx_path="kitti/image_sets/$ARG3.txt"

# Activate conda environment
conda activate fpn

# run the test
if "$ARG4"; then
  echo "Running with intensity enabled"
  python train/test.py --gpu 1 --num_point 1024 --model frustum_pointnets_v1 --model_path "$model_path" --output "$output" --data_path "$data_path" --idx_path "$idx_path" --batch_size 32
else
  echo "Running without intensity"
  python train/test.py --gpu 1 --num_point 1024 --model frustum_pointnets_v1 --model_path "$model_path" --output "$output" --data_path "$data_path" --idx_path "$idx_path" --batch_size 32 --no_intensity
fi

# evaluation
eval_data_path="dataset/KITTI/object/$ARG3/label_2"
eval_output_path="$ARG2"

# train/kitti_eval/evaluate_object_3d_offline "$eval_data_path" "$eval_output_path"

# Deactivate conda environment
conda deactivate
