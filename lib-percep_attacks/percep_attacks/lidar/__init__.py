# @Author: Spencer Hallyburton <spencer>
# @Date:   2021-10-24
# @Filename: __init__.py
# @Last modified by:   spencer
# @Last modified time: 2021-10-24

import sys
import os
import numpy as np
from copy import copy, deepcopy


from avutils.perception import bbox


# ==============================================================================
# POINT CLOUD MANIPULATION
# ==============================================================================

def sample_pc(pc, keep, strategy='random', upsample=False):
    """
    Take points from the full point cloud

    INPUTS:
    pc       --> lidar point cloud
    keep     --> either a fraction on [0,1) or an integeter on [1,inf)
    strategy --> options are: random,
    upsample --> whether upsampling is allowed or if error thrown when too many
    """
    import warnings
    pc2 = np.copy(pc)

    # If there are no points, get out of here
    if pc2.shape[0] == 0:
        return pc2

    if keep > 1:
        # to allow to pass in integer for number of points to keep
        pts_choose = int(keep)
    else:
        # to allow to pass in a fraction
        pts_choose = int(np.round(pc2.shape[0]*keep))

    # Ensure at least 1 point (?)
    pts_choose = max(1, pts_choose)

    # check if too many points
    need_upsample = False
    if (pts_choose > pc2.shape[0]) and not upsample:
        warnings.warn('Number of desired points (%i) is more than the points that exist (%i)...going with max points' %
                      (pts_choose, pc2.shape[0]))
        pts_choose = pc2.shape[0]
    elif (pts_choose > pc2.shape[0]):
        need_upsample = True

    # perform downsampling
    if strategy=='random':
        # Upsample points if needed
        # randomly sample points from the entire space
        if need_upsample:
            # Create additional points with jitter
            nNew = pts_choose - pc2.shape[0]
            idx_new_pts = np.random.choice(pc2.shape[0], size=nNew)
            new_pts = pc2[idx_new_pts,:]
            jf = 0.1  # meters
            jitter = np.array([[jf, 0, 0], [0, jf, 0], [0, 0, jf]])
            new_pts[:,0:3] += np.random.randn(nNew, 3) @ jitter
            # Force bounds
            maxarr = np.max(pc2[:,0:3], axis=0)
            minarr = np.min(pc2[:,0:3], axis=0)
            new_pts[:,0:3] = np.minimum(new_pts[:,0:3], np.tile(maxarr, (nNew,1)))
            new_pts[:,0:3] = np.maximum(new_pts[:,0:3], np.tile(minarr, (nNew,1)))
            pc2 = np.vstack([pc2, new_pts])
        else:
            # Sample from there
            idx_pcs = np.random.choice(pc2.shape[0], size=pts_choose, replace=False)
            pc2 = pc2[idx_pcs, :]
    elif strategy=='exterior':
        # sample points mostly from the exterior of the object
        raise NotImplementedError

    return pc2


def range_out_points(lidar, label, calib, r_new,
                     gp_consistent=False, rescale=False, scale_range=False):
    """
    Slide points along the range dimension, optionally update label

    INPUTS:---------------------------------
    lidar         --> nx3 or nx4 point cloud
    label         --> original label, will output a new one at range
    calib         --> calibration class
    gp_consistent --> whether the output will be made consistent with ground
    rescale       --> whether to rescale the outputs to be the same spread as original
    """

    # Copy the data
    new_lidar = copy(lidar)
    new_label = deepcopy(label)

    # Get 3D box center
    box_center_rect = np.asarray(new_label.box3d.t)
    box_center_velo = calib.project_rect_to_velo(np.expand_dims(box_center_rect,0))[0,:]
    bcv = box_center_velo

    # Manipulate range
    r_0 = np.linalg.norm(box_center_velo)
    bcv2 = copy(box_center_velo)

    # Handle if needs to be ground-plane consistent
    if gp_consistent:
        # Apply shift in velo coordinates to maintain original Z (height) coordinate
        # solving this: rf^2 = z0^2 + (s*y0)^2 + (s*x0)^2
        s = np.sqrt( (r_new**2 - bcv[2]**2) / (bcv[0]**2 + bcv[1]**2) )
        bcv2[0:2] *= s
    else:
        # Apply shift along unit vector and allow to change Z coordinate
        s = r_new / r_0
        bcv2[0:3] *= s

    # Apply change in center coordinates
    delta_box_center = bcv2 - bcv
    new_lidar[:,0:3] += delta_box_center

    # Apply shift to label (3D box)
    box_center_velo += delta_box_center
    box_center_rect = calib.project_velo_to_rect(np.expand_dims(box_center_velo,0))[0,:]
    new_label.box3d.t = box_center_rect

    # If rescaling, scale points toward centerline to make it fit in same frustum
    if rescale:
        rr = r_new / r_0
        # Rotate into a boresight coordinate frame and scale cross-range
        center_vec = box_center_velo
        R = tforms.align_x_to_vec(center_vec)

        # New points: Rinv @ S @ R @ PC (associative property holds)
        # Where S is [[rr, 0, 0], [0, rr, 0], [0, 0, rr]]
        if scale_range:
            S = np.array([[rr, 0, 0], [0, rr, 0], [0, 0, rr]])
        else:
            S = np.array([[1, 0, 0], [0, rr, 0], [0, 0, rr]])

        # Apply scaling
        M = np.transpose(R) @ S @ R
        new_lidar[:,0:3] = (new_lidar[:,0:3]-box_center_velo) @ np.transpose(M) + \
                            box_center_velo

        # Scale the bounding box corners
        # NOTE: this is broken right now...
        box_corners = new_label.box3d.get_corners()
        box_corners_velo = calib.project_rect_to_velo(box_corners)
        corners_scaled_velo = (box_corners_velo - box_center_velo) @ np.transpose(M) +\
                                    box_center_velo
        corners_scaled_rect = calib.project_velo_to_rect(corners_scaled_velo)
        l,w,h = bbox.compute_box_size(corners_scaled_rect, new_label.box3d.ry)
        new_label.box3d.l = l
        new_label.box3d.w = w
        new_label.box3d.l = h

    # Apply shift to label (2D box)
    new_label.box2d = bbox.proj_3d_bbox_to_2d_bbox(new_label.box3d, calib.P)

    return new_lidar, new_label


def azimuth_rotate_points(lidar, delta_az, label=None, calib=None):
    """
    Take point cloud and rotate by some azimuth angle

    Define the "delta_az" as the angle that the point cloud needs to rotate

    NOTE: lidar point cloud is defined as (right-handed)
    x-forward
    y-left
    z-up
    so an "azimuth" rotation is about z using the right-hand-rule for direction

    :lidar - nx3/4 point cloud matrix
    :delta_az - the desired azimuth angle to rotate the point cloud
    :label - label associated with that lidar point cloud
    :calib - calibration data
    """
    import avutils.tforms as tforms

    # Build rotation matrix
    Rz = tforms.rotz(delta_az)

    # ------------------------
    # Update points
    # ------------------------
    new_lidar = copy(lidar)
    new_lidar[:,0:3] = lidar[:,0:3] @ Rz.T

    # ------------------------
    # Update label
    # ------------------------
    if label is not None:
        new_label = deepcopy(label)
        assert(calib is not None)

        # --- 3D box center
        t = calib.project_rect_to_velo(label.box3d.get_center())
        t = calib.project_velo_to_rect(Rz @ t)

        # --- 3D bounding box
        box_corners = calib.project_rect_to_velo(label.box3d.get_corners())
        box_corners = calib.project_velo_to_rect(box_corners @ Rz.T)
        l, w, h = bbox.compute_box_size(box_corners)
        ry = bbox.get_heading() - delta_az
        new_box3d = bbox.Box3D(np.array((h, w, l, t[0], t[1], t[2], ry)))

        # --- 2D bounding box
        new_box2d = new_box3d.project_to_2d_bbox(calib)

        # --- make new label
        new_label.box3d = deepcopy(new_box3d)
        new_label.box2d = deepcopy(new_box2d)

        return new_lidar, new_label
    else:
        return new_lidar
