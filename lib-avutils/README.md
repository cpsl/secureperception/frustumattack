
# Utilities for Autonomy Projects

This repository is a collection of utility classes and functions. It is intended to be used as a submodule in other repositories.


## Installation
Install from directory above.
```
pip install -e ./lib-avutils
```


## Data
For the unit tests, this repository expects a folder with the data structure as follows where each subdirectory has the standard subdirectory structure. The best way to go about this is to make symbolic links to where your data are actually stored.
```
./data
    /KITTI/
    /waymo/
```
**compatibility with the waymo open dataset is not yet achieved**.

## Organization
Nearly everything outside of `data_managers.py` should be independent of the source of data, subject to standardization of the data format itself.

## Unit Tests
In order to ensure consistent functionality and to identify when the API changes warrant a significant update, please implement unit tests for developed utilities.


Unit tests can be run from the top level directory (i.e. inside `lib-av-utils/`) with the following command:
```
pytest -v -cov -s --ignore=data
```

## Version Control Strategy
As mentioned above, this repository is intended to be used as a [git submodule](https://git-scm.com/book/en/v2/Git-Tools-Submodules). For effective use of the submodule, each repository using this repo should create its own branch. Then, when changes are validated and robust, a merge request should be made against the master branch. This is to limit the cross-traffic required when using these utilities across multiple repositories.
