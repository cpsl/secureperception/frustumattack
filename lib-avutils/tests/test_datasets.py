# @Author: Spencer Hallyburton <spencer>
# @Date:   2021-10-20
# @Filename: test_datasets.py
# @Last modified by:   spencer
# @Last modified time: 2021-10-20

import os
import numpy as np
import avutils.datasets as datasets


KITTI_data_dir = os.path.join(os.getcwd(), 'data/test_data/object')


#-------------------------------------------
def test_kitti_object_idxs():
    KOD = datasets.REGISTERED_DATASET_CLASSES['KittiObjectDataset']
    data_folder = os.path.join(KITTI_data_dir, 'training')
    KDM = KOD(KITTI_data_dir, 'training')
    assert np.all(KDM.idxs == [1, 100, 1000])


def test_kitti_register_dataset():
    assert 'KittiObjectDataset' in datasets.REGISTERED_DATASET_CLASSES


def test_kitti_make_new():
    split_1 = 'training'
    split_2 = 'test_copy'
    KOD = datasets.REGISTERED_DATASET_CLASSES['KittiObjectDataset']
    KDM_1 = KOD(KITTI_data_dir, split_1)
    KDM_2 = KDM_1.make_new(KITTI_data_dir, split_2)
    assert KDM_1.idxs[0] != KDM_2.idxs[0]


def test_kitti_copy_data():
    KOD = datasets.REGISTERED_DATASET_CLASSES['KittiObjectDataset']
    data_folder = os.path.join(KITTI_data_dir, 'training')
    data_folder_new = os.path.join(KITTI_data_dir, 'training_TEST')

    # Try with nframes
    KOD.copy_experiment_from_to(data_folder, data_folder_new, nframes=10000)
    KDM = KOD(KITTI_data_dir, 'training_TEST')
    assert np.all(KDM.idxs == [1, 100, 1000])

    # Remove
    assert (os.path.exists(data_folder_new))
    KOD.wipe_experiment(data_folder_new)
    assert (not os.path.exists(data_folder_new))

    # Try with frame list
    KOD.copy_experiment_from_to(data_folder, data_folder_new, frame_list=[1, 100])
    KDM = KOD(KITTI_data_dir, 'training_TEST')
    assert np.all(KDM.idxs == [1, 100])
    KOD.wipe_experiment(data_folder_new)


def test_kitti_get_data():
    KOD = datasets.REGISTERED_DATASET_CLASSES['KittiObjectDataset']
    data_folder = os.path.join(KITTI_data_dir, 'training')
    KDM = KOD(KITTI_data_dir, 'training')

    # Get all data
    data_names = ['lidar', 'image', 'label', 'calibration']
    percep_data_dict = KDM.get_data(KDM.idxs[0])
    assert np.all(np.asarray([dn in percep_data_dict for dn in data_names]))
    assert np.all(np.asarray([percep_data_dict[dn] is not None for dn in data_names]))

    # Get individual data
    for dn in data_names:
        percep_data = KDM.get_data_by_type(KDM.idxs[0], dn)
        assert percep_data is not None

    # Get labels from dir
    dirlab = os.path.join(KDM.split_path, 'label_2')
    assert len(KOD.get_labels_from_directory(dirlab, idxs=None)[0]) == 3
    assert len(KOD.get_labels_from_directory(dirlab, idxs=1)[0]) == 1
    assert len(KOD.get_labels_from_directory(dirlab, idxs=[1,100])[0]) == 2
