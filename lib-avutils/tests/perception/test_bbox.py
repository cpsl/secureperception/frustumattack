# @Author: Spencer Hallyburton <spencer>
# @Date:   2021-02-24
# @Filename: test_bbox_util.py
# @Last modified by:   spencer
# @Last modified time: 2021-08-10

import numpy as np

from copy import copy
import avutils.perception.bbox as bbox
import avutils.perception.evaluation as evaluation


def test_2d_intersection_union():
    # No intersection
    box1 = [100, 200, 200, 300]
    box2 = [201, 301, 300, 400]
    assert(bbox.box_intersection(box1, box2) == 0.0)

    # Partial intersection
    box1 = [1, 1, 10, 10]
    box2 = [1, 6, 10, 15]
    assert(bbox.box_intersection(box1, box2) == bbox.box_union(box1, box2)/3)

    # Full intersection
    box1 = [100, 200, 200, 300]
    assert(bbox.box_intersection(box1, box1) == bbox.box_union(box1, box1))


def test_3d_intersection_union():
    # No intersection
    box1 = np.array([[-15.59353107,   2.39      ,  56.64574515],
                     [-17.46353048,   2.39      ,  56.64425602],
                     [-17.46646893,   2.39      ,  60.33425485],
                     [-15.59646952,   2.39      ,  60.33574398],
                     [-15.59353107,   0.72      ,  56.64574515],
                     [-17.46353048,   0.72      ,  56.64425602],
                     [-17.46646893,   0.72      ,  60.33425485],
                     [-15.59646952,   0.72      ,  60.33574398]])
    box2 = np.array([[ 4.31106765,  1.32      , 46.85602005],
                     [ 4.91093791,  1.32      , 46.84354315],
                     [ 4.86893235,  1.32      , 44.82397995],
                     [ 4.26906209,  1.32      , 44.83645685],
                     [ 4.31106765, -0.54      , 46.85602005],
                     [ 4.91093791, -0.54      , 46.84354315],
                     [ 4.86893235, -0.54      , 44.82397995],
                     [ 4.26906209, -0.54      , 44.83645685]])
    assert(bbox.box_intersection(box1, box2) == 0.0)

    # Partial intersection
    assert(bbox.box_intersection(box1, box1+1.0) > 0.0)


def test_same_intersection_union():
    # Full intersection
    box1 = np.array([[-15.59353107,   2.39      ,  56.64574515],
                     [-17.46353048,   2.39      ,  56.64425602],
                     [-17.46646893,   2.39      ,  60.33425485],
                     [-15.59646952,   2.39      ,  60.33574398],
                     [-15.59353107,   0.72      ,  56.64574515],
                     [-17.46353048,   0.72      ,  56.64425602],
                     [-17.46646893,   0.72      ,  60.33425485],
                     [-15.59646952,   0.72      ,  60.33574398]])
    assert(abs(bbox.box_intersection(box1, box1) -
                bbox.box_union(box1, box1) < 1e-8))
