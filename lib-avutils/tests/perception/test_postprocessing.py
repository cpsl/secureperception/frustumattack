# @Author: Spencer Hallyburton <spencer>
# @Date:   2021-07-28
# @Filename: test_postprocessing.py
# @Last modified by:   spencer
# @Last modified time: 2021-07-30

import os
import shutil
from avutils.perception import postprocessing
import avutils.datasets as datasets

KITTI_data_dir = os.path.join(os.getcwd(), 'data/test_data/object')
post_data_dir = os.path.join(os.getcwd(), 'data/test_data/postprocess_test')


def set_up_test_data():
    # Copy the data folder locally, if it doesn't exist
    if not os.path.exists(post_data_dir):
        shutil.copytree(KITTI_data_dir, post_data_dir)


def clean_up_test_data():
    # Remove the data folder locally
    shutil.rmtree(post_data_dir)


def get_all_labels(postproc=False):

    KOD = datasets.REGISTERED_DATASET_CLASSES['KittiObjectDataset']
    KDM = KOD(post_data_dir, 'training')

    if postproc:
        append_str = '_POSTPROCESSED'
    else:
        append_str = ''
    data_path = os.path.join(KDM.split_path, 'label_2' + append_str)
    new_labs, _ = KOD.get_labels_from_directory(data_path, idxs=KDM.idxs)
    assert(len(new_labs) > 0)
    return new_labs


def test_no_operation():
    set_up_test_data()
    KOD = datasets.REGISTERED_DATASET_CLASSES['KittiObjectDataset']

    try:
        KDM = KOD(post_data_dir, 'training')
        labels_pre_all = get_all_labels()

        # Do postprocessing
        sdir = os.path.join(post_data_dir, 'training', 'label_2')
        postprocessing.run_3D_detection_postprocessing(KDM, os.path.join(post_data_dir, 'training', 'label_2'))

        # Check that the labels are the same
        labels_post_all = get_all_labels(postproc=True)
        for labels_pre, labels_post in zip(labels_pre_all, labels_post_all):
            for l1, l2 in zip(labels_pre, labels_post):
                assert(l1 == l2)
    finally:
        clean_up_test_data()


def test_range_limit():
    set_up_test_data()
    KOD = datasets.REGISTERED_DATASET_CLASSES['KittiObjectDataset']

    try:
        KDM = KOD(post_data_dir, 'training')
        labels_pre_all = get_all_labels()
        sdir = os.path.join(post_data_dir, 'training', 'label_2')

        # Manipulate the range of some
        for i, labels_pre in enumerate(labels_pre_all):
            if len(labels_pre) > 0:
                labels_pre[0].box3d.t += 10000
            # Save labels
            KDM.save_labels(labels_pre, sdir, KDM.idxs[i], add_label_folder=False)

        # Do postprocessing
        postprocessing.run_3D_detection_postprocessing(KDM, os.path.join(post_data_dir, 'training','label_2'), range_filter=True, max_range=100)

        # Check the results
        labels_post_all = get_all_labels(postproc=True)
        for labels_pre, labels_post in zip(labels_pre_all, labels_post_all):
            if len(labels_pre) > 0:
                assert(len(labels_pre) == (len(labels_post) + 1))
    finally:
       clean_up_test_data()
