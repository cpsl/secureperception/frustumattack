# @Author: Spencer Hallyburton <spencer>
# @Date:   2021-02-24
# @Filename: test_evaluation.py
# @Last modified by:   spencer
# @Last modified time: 2021-08-18

import os
import numpy as np
import avutils.perception.evaluation as evaluation
import avutils.datasets as datasets



KITTI_data_dir = os.path.join(os.getcwd(), 'data/test_data/object')

def test_detection_truth_assignment():
    KOD = datasets.REGISTERED_DATASET_CLASSES['KittiObjectDataset']
    KDM = KOD(KITTI_data_dir, 'training')
    idx_frame = 100

    # Make truths and detections
    truths = KDM.get_labels(idx_frame)
    detections = truths[2:]
    detections = np.concatenate((detections, [KDM.get_labels(1)[0]]))

    # Run assignment
    def run_assignment(metric, FN_expect, radius=None):
        assignment, idx_FP, idx_FN, cost = evaluation.associate_detections_truths(detections, truths, metric=metric, radius=radius)

        # Check results
        assert(len(idx_FP) == 1)
        assert(idx_FP[0] == len(detections)-1)
        assert(len(idx_FN) == FN_expect)
        if FN_expect == 2:
            assert(idx_FN[0] == 0)
            assert(idx_FN[1] == 1)
        elif FN_expect == 1:
            assert(idx_FN[0] == 1)
        else:
            raise RuntimeError

    run_assignment('3D_IoU', FN_expect=2)
    run_assignment('center_dist', radius=2, FN_expect=2)
    run_assignment('center_dist', radius=16, FN_expect=2)


def test_2d_iou():
    # Partial intersection
    box1 = [1, 1, 10, 10]
    box2 = [1, 6, 10, 15]
    assert(evaluation.IOU_2d(box1, box2) == 1/3)


def test_3d_iou():
    # Partial intersection
    box1 = np.array([[-15.59353107,   2.39      ,  56.64574515],
                     [-17.46353048,   2.39      ,  56.64425602],
                     [-17.46646893,   2.39      ,  60.33425485],
                     [-15.59646952,   2.39      ,  60.33574398],
                     [-15.59353107,   0.72      ,  56.64574515],
                     [-17.46353048,   0.72      ,  56.64425602],
                     [-17.46646893,   0.72      ,  60.33425485],
                     [-15.59646952,   0.72      ,  60.33574398]])

    assert(evaluation.IOU_3d(box1, box1+1.0) > 0.0)


def test_detection_result():
    # Get data
    KOD = datasets.REGISTERED_DATASET_CLASSES['KittiObjectDataset']
    KDM = KOD(KITTI_data_dir, 'training')
    idx_frame = 100

    # Make truths and detections
    truths = KDM.get_labels(idx_frame)
    detections = truths[2:]
    detections = np.concatenate((detections, [KDM.get_labels(1)[0]]))

    # Make detection result
    DR = evaluation.DetectionResult(detections, truths, idx_frame)
    assert(DR.confusion[0,0] == len(truths)-2)
    assert(DR.confusion[1,0] == 1)
    assert(DR.confusion[0,1] == 2)
    assert(np.all(DR.get_indices_of('false_positives') == np.array([len(detections)-1])))
    assert(np.all(DR.get_indices_of('false_negatives') == np.array([0,1])))
    assert(DR.get_indices_of('assigned')[0] == (0, 2))
    assert(len(DR.get_assignment_iou()) == len(truths)-2)
    assert(DR.get_assignment_iou(idx_det=0) == DR.get_assignment_iou(idx_tru=2))
    assert(DR.get_assignment_iou(idx_det=0) > 0)


def test_results_from_folder():
    KOD = datasets.REGISTERED_DATASET_CLASSES['KittiObjectDataset']
    KDM = KOD(KITTI_data_dir, 'training')
    res_path = os.path.join(KITTI_data_dir, 'training', 'label_2')

    # Run without multiprocess
    # det_res, conf_mat = evaluation.get_detection_results_from_folder(KDM, res_path)

    # Run with multiprocess
    det_res, conf_mat = evaluation.get_detection_results_from_folder(KDM, res_path, metric='3D_IoU')
    assert len(det_res) == 3
    assert conf_mat.shape == (2,2,3)
    idx_frame = 100
    assert det_res[idx_frame].get_objects_of('truths')[0] == KDM.get_labels(idx=idx_frame, whitelist_types=['Car'])[0]
    assert np.sum(conf_mat, axis=2)[0,1] == 0
    assert np.sum(conf_mat, axis=2)[1,0] == 0
