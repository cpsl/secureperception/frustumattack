# @Author: Spencer Hallyburton <spencer>
# @Date:   2021-02-24
# @Filename: test_maskfilters.py
# @Last modified by:   spencer
# @Last modified time: 2021-08-03


import os
import sys
import numpy as np
from copy import copy, deepcopy


import avutils.datasets as datasets
import avutils.perception.maskfilters as maskfilters

KITTI_data_dir = os.path.join(os.getcwd(), 'data/test_data/object')

#------Get KITTI data for the tests
KOD = datasets.REGISTERED_DATASET_CLASSES['KittiObjectDataset']
KDM = KOD(KITTI_data_dir, 'training')

idx_frame_extract = 1000
img = KDM.get_image(idx_frame_extract)
pc = KDM.get_lidar(idx_frame_extract)
cal = KDM.get_calibration(idx_frame_extract)
lab = KDM.get_labels(idx_frame_extract)[0]


def test_filter_frustum():
    frustum_filter = maskfilters.filter_points_in_frustum(pc, lab.box2d, cal)
    assert(sum(frustum_filter) > 0)
    assert(max(np.where(frustum_filter)[0]) < pc.shape[0])


def test_filter_bbox():
    bbox_filter = maskfilters.filter_points_in_object_bbox(pc, lab.box3d, cal)
    assert(sum(bbox_filter) > 0)
    assert(max(np.where(bbox_filter)[0]) < pc.shape[0])


def test_single_in_frustum_1():
    # Test underlying function
    lab2 = deepcopy(lab)
    lab2.box3d.t += 1
    lab2.box2d = lab2.box3d.project_to_2d_bbox(calib=cal)
    assert(maskfilters._item_in_frustum(lab, lab2, cal))


def test_single_in_frustum_2():
    # Test interface function
    lab2 = deepcopy(lab)
    lab2.box3d.t += 1
    lab2.box2d = lab2.box3d.project_to_2d_bbox(calib=cal)
    assert(maskfilters.filter_objects_in_frustum(lab, lab2, cal)[0,0])
    # Test interface function
    lab2 = deepcopy(lab)
    lab2.box3d.t[1] += 30
    lab2.box2d = lab2.box3d.project_to_2d_bbox(calib=cal)
    assert(not maskfilters.filter_objects_in_frustum(lab, lab2, cal)[0,0])


def test_multiple_in_frustum():
    # Test interface function
    lab2 = deepcopy(lab)
    lab2.box3d.t += 1
    lab2.box2d = lab2.box3d.project_to_2d_bbox(calib=cal)
    lab3 = deepcopy(lab)
    lab3.box3d.t += 30
    lab3.box2d = lab3.box3d.project_to_2d_bbox(calib=cal)
    res = maskfilters.filter_objects_in_frustum(lab, [lab2, lab3], cal)
    assert(res[0,0])
    assert(not res[0,1])


def test_range_filter():
    assert(sum(maskfilters.filter_points_range(pc, np.inf)) == pc.shape[0])
    assert(sum(maskfilters.filter_points_range(pc, -1)) == 0)


def test_shadow_filter():
    shadow_filter = maskfilters.filter_points_in_shadow(pc, lab.box2d, lab.box3d, cal)
    assert(sum(shadow_filter) > 0)

    lab2 = deepcopy(lab)
    lab2.box3d.t[0] += 3
    lab2.box2d = lab2.box3d.project_to_2d_bbox(calib=cal)
    shadow_filter2 = maskfilters.filter_points_in_shadow(pc, lab2.box2d, lab2.box3d, cal)
    assert(sum(shadow_filter2) > 0)
    assert(sum(shadow_filter2) > sum(shadow_filter))
