# @Author: Spencer Hallyburton <spencer>
# @Date:   2021-10-19
# @Filename: algorithms.py
# @Last modified by:   spencer
# @Last modified time: 2021-10-19

import abc
import os
import glob
import psutil
import subprocess
from shutil import copyfile, rmtree
import time


REGISTERED_ALGORITHM_CLASSES = {}

def register_algorithm(cls, name=None):
    global REGISTERED_ALGORITHM_CLASSES
    if name is None:
        try:
            names = cls.acceptable_names
        except Exception:
            names = [cls.__name__]
    for name in names:
        name = name.lower()
        assert name not in REGISTERED_ALGORITHM_CLASSES, f"exist class: {REGISTERED_ALGORITHM_CLASSES}"
        REGISTERED_ALGORITHM_CLASSES[name] = cls
    return cls

def get_algorithm_class(name):
    global REGISTERED_ALGORITHM_CLASSES
    name = name.lower()
    assert name in REGISTERED_ALGORITHM_CLASSES, f"available class: {REGISTERED_ALGORITHM_CLASSES}"
    return REGISTERED_ALGORITHM_CLASSES[name]


# IMPORTANT: assumes that the algorithms are placed in the directory
# above lib-avutils in a folder called: algorithms/XXX
this_dir = os.path.dirname(os.path.abspath(__file__))
dir_top = os.path.dirname(os.path.dirname(os.path.dirname(this_dir)))
dir_algs = os.path.join(dir_top, 'algorithms')
assert os.path.exists(dir_algs), "Cannot find algorithms directory at {}...make sure standard is followed".format(dir_algs)


# ===============================================================
# ABSTRACT BASE CLASSES
# ===============================================================

class PerceptionAlgorithm():

    @classmethod
    def get_save_dir(cls, experiment):
        return os.path.join(os.getcwd(), 'results', f'{cls.primary_name}-{experiment}', 'data/')

    @staticmethod
    def validate_experiment(experiment):
        assert 'experiment' in experiment

    @abc.abstractmethod
    def inference_on_folder(self, folder):
        """Run inference on items in a folder"""

    @abc.abstractmethod
    def inference(self, data):
        """Run inference on a data element"""

    @classmethod
    def inference_on_folder_run_command(cls, folder, save_override=None, **kwargs):
        cls.prepare_data_run_command(folder, **kwargs)
        cls.call_run_command(folder, save_override, **kwargs)

    @classmethod
    def prepare_data_run_command(cls, folder,  remove_res=True, **kwargs):
        experiment = folder.split('/')[-1]
        cls.validate_experiment(experiment)
        sdir = cls.get_save_dir(experiment)
        if remove_res and os.path.exists(sdir):
            rmtree(sdir)
        cls._prepare_data_run_command(folder, experiment, sdir, **kwargs)

    @abc.abstractmethod
    def _prepare_data_run_command(cls, folder, experiment, **kwargs):
        """prepares data before running"""

    @classmethod
    def call_run_command(cls, folder, save_override=None, **kwargs):
        experiment = folder.split('/')[-1]
        cls.validate_experiment(experiment)
        if save_override is None:
            sdir = cls.get_save_dir(experiment)
        else:
            sdir = save_override
        cls._call_run_command(folder, experiment, sdir, **kwargs)

    @abc.abstractmethod
    def _call_run_command(cls, folder, experiment, sdir, **kwargs):
        """calls algorithm"""


# ===============================================================
# Depth Estimation
# ===============================================================

class DepthEstimation(PerceptionAlgorithm):
    alg_dir = ''

    def inference_on_folder(self, folder):
        raise NotImplementedError

    def inference(self, data):
        raise NotImplementedError

# ------------------------------------
# Specific Implementations
# ------------------------------------

@register_algorithm
class Monodepth2(DepthEstimation):
    alg_dir = os.path.join(dir_algs, 'monodepth2')
    acceptable_names = ['monodepth', 'monodepth2']
    primary_name = 'monodepth2'

    @classmethod
    def _prepare_data_run_command(cls, folder, experiment, sdir, **kwargs):
        pass

    @classmethod
    def _call_run_command(cls, folder, experiment, sdir, save_local=False, as_depth=True, **kwargs):
        assert(as_depth)  # for now...
        call_str = ['bash', '-i', './scripts/test_monodepth_experiment.sh', experiment]
        if save_local:
            call_str.append(sdir)
        run_command(call_str, dirchange=cls.alg_dir)


# ===============================================================
# Depth Estimation
# ===============================================================

class SemanticSegmentation(PerceptionAlgorithm):
    alg_dir = ''

    def inference_on_folder(self, folder):
        raise NotImplementedError

    def inference(self, data):
        raise NotImplementedError

# ------------------------------------
# Specific Implementations
# ------------------------------------

@register_algorithm
class LUNet(SemanticSegmentation):
    alg_dir = os.path.join(dir_algs, 'lunet')
    acceptable_names = ['lunet']
    primary_name = 'lunet'

    @classmethod
    def _prepare_data_run_command(cls, folder, experiment, sdir, method, **kwargs):
        pass

    @classmethod
    def _call_run_command(cls, folder, experiment, sdir, method, **kwargs):
        assert method in ['lunet', 'force'], f'Cannot run method {method}'
        call_str = ['bash', '-i', './lunet_augmentation.sh', experiment, method]
        run_command(call_str, dirchange=cls.alg_dir)


# ===============================================================
# 2D Detector Implementations
# ===============================================================

class ObjectDetection2D(PerceptionAlgorithm):
    alg_dir = ''

    def inference_on_folder(self, folder):
        raise NotImplementedError

    def inference(self, data):
        raise NotImplementedError


@register_algorithm
class Detectron2(ObjectDetection2D):
    alg_dir = os.path.join(dir_algs, 'detectron2')
    acceptable_names = ['detectron', 'detectron2']
    primary_name = 'detectron2'

    @classmethod
    def _prepare_data_run_command(cls, folder, experiment, sdir, **kwargs):
        pass

    @classmethod
    def _call_run_command(cls, folder, experiment, sdir, model='model_final', **kwargs):
        call_str = ['bash', '-i', './scripts/run_kitti_test.sh', model, experiment, sdir]
        run_command(call_str, dirchange=cls.alg_dir)


# ===============================================================
# 3D Detector Implementations
# ===============================================================

class ObjectDetection3D(PerceptionAlgorithm):
    alg_dir = ''

    def inference_on_folder(self, folder):
        raise NotImplementedError

    def inference(self, data):
        raise NotImplementedError


# ------------------------------------
# Specific Implementations
# ------------------------------------

@register_algorithm
class AVOD(ObjectDetection3D):
    alg_dir = os.path.join(dir_algs, 'avod')
    acceptable_names = ['avod']
    primary_name = 'avod'

    @classmethod
    def _prepare_data_run_command(cls, folder, experiment, sdir, **kwargs):
        pass

    @classmethod
    def _call_run_command(cls, folder, experiment, sdir, **kwargs):
        sdir = cls.get_save_dir(experiment)
        checkpoint = 'pyramid_cars_with_aug_example'
        call_str = ['bash', '-i', './scripts/test_custom.sh', experiment, sdir, checkpoint]
        run_command(call_str, dirchange=cls.alg_dir)


@register_algorithm
class Deepbox(ObjectDetection3D):
    alg_dir = os.path.join(dir_algs, '3d-deepbox')
    acceptable_names = ['deepbox', '3d-deepbox']
    primary_name = 'deepbox'

    @classmethod
    def _prepare_data_run_command(cls, folder, experiment, sdir, **kwargs):
        pass

    @classmethod
    def _call_run_command(cls, folder, experiment, sdir, model='model-50', detection_path=None, **kwargs):
        model_dir = os.path.join('model', model)
        call_str = ['bash', '-i', './scripts/test.sh', model_dir, sdir, experiment]
        if detection_path is not None:
            call_str.append(detection_path)
        run_command(call_str, dirchange=cls.alg_dir)


@register_algorithm
class EPNET(ObjectDetection3D):
    alg_dir = os.path.join(dir_algs, 'epnet')
    acceptable_names = ['epnet']
    primary_name = 'epnet'

    @classmethod
    def _prepare_data_run_command(cls, folder, experiment, sdir, **kwargs):
        pass

    @classmethod
    def _call_run_command(cls, folder, experiment, sdir, checkpoint='full_epnet_with_iou_branch', thresh_rcnn=0.4, thresh_rpn=0.4, **kwargs):
        call_str = ['bash', '-i', 'run_custom_eval.sh', experiment, sdir, checkpoint, str(thresh_rcnn), str(thresh_rpn)]
        run_command(call_str, dirchange=os.path.join(cls.alg_dir, 'tools'))


@register_algorithm
class FrustumConvNet(ObjectDetection3D):
    alg_dir = os.path.join(dir_algs, 'frustum-convnet')
    acceptable_names = ['fcn', 'frustumconvnet', 'frustum-convnet']
    primary_name = 'fcn'

    @classmethod
    def _prepare_data_run_command(cls, folder, experiment, sdir, **kwargs):
        call_str = ['bash', '-i', './scripts/prepare_custom_data.sh', experiment]
        run_command(call_str, dirchange=cls.alg_dir)

    @classmethod
    def _call_run_command(cls, folder, experiment, sdir, model='standard', do_refine=False, **kwargs):
        refine_str = 'true' if do_refine else 'false'
        if model == 'standard':
            model_dir = os.path.join('pretrained_models/car/', 'model_0050.pth')
        elif model == 'mono':
            model_dir = os.path.join('output/car_train_from_mono/', 'model_0050.pth')
        else:
            raise NotImplementedError
        call_str = ['bash', '-i', './scripts/eval_custom_experiment.sh',
                     model_dir, sdir, experiment, refine_str]
        run_command(call_str, dirchange=cls.alg_dir)


@register_algorithm
class FrustumPointNet(ObjectDetection3D):
    alg_dir = os.path.join(dir_algs, 'frustum-pointnets')
    acceptable_names = ['fpn', 'frustumpointnet', 'frustum-pointnets']
    primary_name = 'fpn'

    @classmethod
    def _prepare_data_run_command(cls, folder, experiment, sdir, **kwargs):
        call_str = ['bash', '-i', './scripts/command_prep_custom_data.sh', experiment]
        run_command(call_str, dirchange=cls.alg_dir)

    @classmethod
    def _call_run_command(cls, folder, experiment, sdir, model='standard', with_intensity=True, **kwargs):
        inten_str = 'true' if with_intensity else 'false'
        if model == 'standard':
            log_str = 'log_v1'
        elif model == 'mono':
            log_str = 'log_v1_from_mono'
        if not with_intensity:
            sdir += '_NOINTENSITY'
            log_str += '_NOINTENSITY'
        call_str = ['bash', '-i', './scripts/command_test_experiment_B.sh', log_str, sdir, experiment, inten_str]
        run_command(call_str, dirchange=cls.alg_dir)


@register_algorithm
class PointPillars(ObjectDetection3D):
    alg_dir = os.path.join(dir_algs, 'second/second/')
    acceptable_names = ['pillars', 'pointpillars']
    primary_name = 'pillars'

    @classmethod
    def _prepare_data_run_command(cls, folder, experiment, sdir, svf=False, svf_method=None, **kwargs):
        # -- augment with lunet
        if svf:
            lunet = get_algorithm_class('LUNet')
            lunet.inference_on_folder_run_command(folder, remove_res=False, method=svf_method)

        # -- run pillars augmentation
        call_str = ['bash', '-i', './custom_data_prep.sh', experiment]
        run_command(call_str, dirchange=cls.alg_dir)

    @classmethod
    def _call_run_command(cls, folder, experiment, sdir, svf=False, svf_method=None, **kwargs):
        if svf:
            print('Running using SVF augmentation')
            if svf_method == 'lunet':
                model = 'last_train_svf_lunet'
            elif svf_method == 'force':
                model = 'last_train_svf_force'
            else:
                raise NotImplementedError
        else:
            print('Running standard pillars')
            model = 'last_train'

        call_str = ['bash', '-i', './custom_inference.sh', model, sdir, experiment]
        run_command(call_str, dirchange=cls.alg_dir)


@register_algorithm
class Pixor(ObjectDetection3D):
    alg_dir = os.path.join(dir_algs, 'pixor')
    acceptable_names = ['pixor']
    primary_name = 'pixor'

    @classmethod
    def _prepare_data_run_command(cls, folder, experiment, sdir, **kwargs):
        pass

    @classmethod
    def _call_run_command(cls, folder, experiment, sdir, **kwargs):
        call_str = ['bash', '-i', './run_experiment.sh', experiment, sdir]
        run_command(call_str, dirchange=cls.alg_dir)


@register_algorithm
class PointRCNN(ObjectDetection3D):
    alg_dir = os.path.join(dir_algs, 'pointrcnn')
    acceptable_names = ['pointrcnn', 'point-rcnn']
    primary_name = 'pointrcnn'

    @classmethod
    def _prepare_data_run_command(cls, folder, experiment, sdir, **kwargs):
        pass

    @classmethod
    def _call_run_command(cls, folder, experiment, sdir, **kwargs):
        call_str = ['bash', '-i', './run_custom_eval.sh', experiment, sdir]
        run_command(call_str, dirchange=os.path.join(cls.alg_dir, 'tools'))


@register_algorithm
class PseudoLidar(ObjectDetection3D):
    alg_dir = os.path.join(dir_algs, 'pseudo_lidar')
    acceptable_names = ['pseudolidar', 'pseudo_lidar', 'pseudo-lidar']
    primary_name = 'pseudolidar'

    @classmethod
    def _prepare_data_run_command(cls, folder, experiment, sdir, **kwargs):
        pass

    @classmethod
    def _call_run_command(cls, folder, experiment, sdir, image_algorithm, lidar_algorithm, image_as_depth=True, **kwargs):
        assert image_algorithm.lower() in ['monodepth2']
        assert lidar_algorithm.lower() in ['fpn', 'fcn']

        # Get algorithms first
        im_alg = get_algorithm_class(image_algorithm)
        li_alg = get_algorithm_class(lidar_algorithm)

        print('\nRUNNING DISPARITY/DEPTH ALGORITHM')
        im_alg.inference_on_folder_run_command(folder, remove_res=False)

        print('\nMAKING LIDAR DATA FROM DISPARITY/DEPTH')
        call_str = ['bash', '-i', './scripts/make_pseudo_lidar.sh', experiment]
        if image_as_depth:
            call_str.append('true')
        run_command(call_str, dirchange=cls.alg_dir)

        print('nRUNNING LIDAR ALGORITHM')
        li_alg.inference_on_folder_run_command(folder, save_override=sdir, remove_res=False, model='mono')


@register_algorithm
class RTM3D(ObjectDetection3D):
    alg_dir = os.path.join(dir_algs, 'RTM3D')
    acceptable_names = ['rtm3d']
    primary_name = 'rtm3d'

    @classmethod
    def _prepare_data_run_command(cls, folder, experiment, sdir, **kwargs):
        pass

    @classmethod
    def _call_run_command(cls, folder, experiment, sdir, **kwargs):
        model='res_18'
        base_dir = folder.replace(folder.split('/')[-1], '')
        call_str = ['bash', '-i', './custom_inference.sh', base_dir, experiment, sdir, model]
        run_command(call_str, dirchange=cls.alg_dir)


# ===============================================================
# UTILITIES TO RUN
# ===============================================================

def kill_process(proc_pid):
    process = psutil.Process(proc_pid)
    for proc in process.children(recursive=True):
        proc.kill()
    process.kill()


def run_command(call_list, dirchange=None):
    curdir = os.getcwd()
    if dirchange is not None:
        os.chdir(dirchange)
    try:
        print('STARTING PROCESS')
        pro = subprocess.Popen(call_list, shell=False, preexec_fn=os.setsid)
        pro.wait()
    except KeyboardInterrupt:
        print('KILLING PROCESS')
        kill_process(pro.pid)
    else:
        print('FINISHED PROCESS')
    finally:
        os.chdir(curdir)


def copy_src_dest(src_folder, dest_folder, common_suffix):
    src_file = os.path.join(src_folder, common_suffix)
    dest_file = os.path.join(dest_folder, common_suffix)
    copyfile(src_file, dest_file)
