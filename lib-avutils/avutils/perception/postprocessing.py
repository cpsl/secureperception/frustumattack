# @Author: Spencer Hallyburton <spencer>
# @Date:   2021-07-28
# @Filename: postprocessing.py
# @Last modified by:   spencer
# @Last modified time: 2021-08-09

"""
Postprocessing on detection results to filter out obviously bad results
"""

import os
from tqdm import tqdm
import numpy as np
from avutils.perception import maskfilters


def run_3D_detection_postprocessing(DM, results_dir, range_filter=False, max_range=100):
    """
    Wrapper for post-processing to perform loading and saving around it
    """
    print('Running postprocessing on all files from - %s' % results_dir)

    # Check directory
    if (not os.path.exists(results_dir)):
        print(results_dir)
        raise RuntimeError('Cannot find data directory')
    elif len(os.listdir(results_dir)) == 0:
        print(results_dir)
        raise RuntimeError('Data directory is empty')

    if results_dir[-1] == '/':
        results_dir_save = results_dir[:-1] + '_POSTPROCESSED'
    else:
        results_dir_save = results_dir + '_POSTPROCESSED'

    # Get labels
    detections_all, idx_all = DM.get_labels_from_directory(results_dir)

    # Postprocess
    detections_valid_all, nrej_all = post_process_3D_detections(detections_all,
                                                                range_filter=range_filter,
                                                                max_range=max_range)

    # Loop for saving
    print('Rejected %i in post-processing' % nrej_all)
    for idx_this, dets_valid in zip(idx_all, detections_valid_all):
        DM.save_labels(dets_valid, results_dir_save, idx_this, add_label_folder=False)


def post_process_3D_detections(detections, range_filter=False, max_range=100):
    """
    Add postprocessing to a 3D detection result
    """
    if len(detections) == 0:
        return detections, 0

    # Recurse to handle list of lists
    if (type(detections[0]) is list) or (type(detections[0]) is np.ndarray):
        detections_valid = []
        nrej = 0
        for dets in detections:
            dets_valid, nrej_this = post_process_3D_detections(dets,
                                                          range_filter=range_filter,
                                                          max_range=max_range)
            detections_valid.append(dets_valid)
            nrej += nrej_this
    else:
        # ------------
        # Operations
        # ------------
        if range_filter:
            in_range = maskfilters.filter_labels_range(detections, max_range)
        else:
            in_range = np.ones((len(detections),), dtype=bool)
        # ------------
        # Get results
        # ------------
        valid = in_range
        detections_valid = [det for i, det in enumerate(detections) if valid[i]]
        nrej = len(valid) - sum(valid)

    return detections_valid, nrej
