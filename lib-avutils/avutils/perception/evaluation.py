# -*- coding: utf-8 -*-
# @Author: spencer
# @Date:   2020-02-04
# @Last modified by:   spencer
# @Last Modified date: 2021-02-04
# @Description:get_detection_results_from_folder
"""
Helper functions for evaluation of perception algorithms
"""
from multiprocessing import Pool, cpu_count
from copy import copy, deepcopy
from functools import partial

import glob
import os
import numpy as np
from tqdm import tqdm

from avutils.perception import bbox
from avutils.perception import visualize

import matplotlib.pyplot as plt

# ==============================================================================
# Detection Result Manager
# ==============================================================================

class DetectionResult():
    """Single-frame detection manager"""
    def __init__(self, detections, truths, idx, metric='3D_IoU', truths_dontcare=[]):
        self.idx = idx
        self.metric = metric

        # Prep value
        self.detections = detections
        self.truths = truths
        self.truths_dontcare = truths_dontcare
        self.truths_all = np.concatenate((truths, np.asarray(truths_dontcare)), axis=0)

        # Run assignment
        self.run_assignment()

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return 'Kitti Detection Result frame {}:\n----{} detections, {} truths\n----{} detections are dontcares\n----{} Assignments, {} FPs, {} FNs'.format(self.idx, len(self.detections), len(self.truths), len(self.result['det_dontcare']), len(self.result['assigned']), len(self.result['false_positives']), len(self.result['false_negatives']))

    def get_confusion(self):
        """
        [[nTP, nFN]
         [nFP, nTN]]
        """
        return self.confusion

    def run_assignment(self):
        # Initialize variables
        self.result = {'false_positives':[], 'false_negatives':[], 'assigned':[], 'det_dontcare':[]}
        self.confusion = np.zeros((2,2))

        # Run assignment
        assignments, idx_FP, idx_FN, _ = associate_detections_truths(self.detections, self.truths_all, self.metric)

        # Remove assignments and FNs in dontcare
        idx_tru_dontcare = list(range(len(self.truths), len(self.truths)+len(self.truths_dontcare)))
        idx_det_dontcare = [a[0] for a in assignments if a[1] in idx_tru_dontcare]
        assignments = [a for a in assignments if a[1] not in idx_tru_dontcare]
        idx_FN = [idx for idx in idx_FN if idx not in idx_tru_dontcare]

        # Make confusion matrix
        self.confusion = np.array([[len(assignments), len(idx_FN)],
                                    [len(idx_FP), 0]])

        # Store map from index to what it is
        self.result['false_positives'] = idx_FP
        self.result['false_negatives'] = idx_FN
        self.result['det_dontcare'] = idx_det_dontcare
        self.result['assigned'] = assignments
        iou_assign = []

        # Get IoU for each assignment
        for ia, ib in self.result['assigned']:
            iou_assign.append(IOU_3d(self.detections[ia].box3d.corners, self.truths[ib].box3d.corners))
        self.result['assigned_iou'] = np.asarray(iou_assign)

    def get_assignment_iou(self, idx_det=None, idx_tru=None):
        dk = list(dict(self.result['assigned']).keys())
        dv = list(dict(self.result['assigned']).values())

        if (idx_det is None) and (idx_tru is None):
            iou = self.result['assigned_iou']
        elif (idx_det is not None) and (idx_tru is None):
            if idx_det in dk:
                iou = self.result['assigned_iou'][dk.index(idx_det)]
            else:
                iou = None
        elif (idx_det is None) and (idx_tru is not None):
            if idx_tru in dv:
                iou = self.result['assigned_iou'][dv.index(idx_tru)]
            else:
                iou = None
        else:
            found = False
            for iou, (i, j) in zip(self.result['assigned_iou'], self.result['assigned']):
                if (i == idx_det) and (j == idx_tru):
                    found = True
                    break
            if not found:
                iou = None
        return iou

    def get_number_of(self, field):
        if field in ['false_positives', 'false_negatives', 'assigned', 'det_dontcare']:
            return len(self.result[field])
        elif field == 'detections':
            return len(self.detections)
        elif field == 'truths':
            return len(self.truths)
        else:
            raise IOError('Cannot understand %s field type' % field)

    def get_indices_of(self, field):
         if field in ['false_positives', 'false_negatives', 'assigned', 'det_dontcare']:
             return self.result[field]
         elif field in ['detections', 'truths']:
             return list(range(self.get_number_of(field)))
         else:
             raise IOError('Cannot understand %s field type' % field)

    def get_objects_of(self, field):
         if field == 'false_positives':
             return [det for i, det in enumerate(self.detections) if i in self.result['false_positives']]
         elif field == 'false_negatives':
             return [tru for i, tru in enumerate(self.truths) if i in self.result['false_negatives']]
         elif field == 'assigned':
             return [(self.detections[i], self.truths[j]) for i, j in self.result['assigned']]
         elif field == 'truths':
             return self.truths
         elif field == 'detections':
             return self.detections
         else:
             raise IOError('Cannot understand %s field type' % field)

    def remove_truth(self, label=None, idx=None):
        assert((label is None) or (idx is None))
        if label is not None:
            if self.truths is list:
                self.truths.remove(label)
            else:
                self.truths = np.delete(self.truths, label)
        elif idx is not None:
            del self.truths[idx]
        else:
            raise IOError('Must pass in something')
        self.run_assignment()

    def remove_detection(self, label=None, idx=None):
        """Remove detection and rerun assignment"""
        assert((label is None) or (idx is None))
        if label is not None:
            if self.detections is list:
                self.detections.remove(label)
            else:
                self.detections = np.delete(self.detections, label)
        elif idx is not None:
            del self.detections[idx]
        else:
            raise IOError('Must pass in something')
        self.run_assignment()

    def visualize(self, DM=None, image=None, lidar=None, calib=None, projection='bev'):
         # Get colors
         labels_all = np.hstack([self.detections, self.truths])
         colors_all = []
         for i in range(len(self.detections)):
             if i in self.result['false_positives']:
                 colors_all.append('red')
             elif i in self.result['det_dontcare']:
                 colors_all.append('cyan')
             else:
                 colors_all.append('blue')
         for i in range(len(self.truths)):
             if i in self.result['false_negatives']:
                 colors_all.append('yellow')
             else:
                 colors_all.append('white')
         # Print color key
         print('White  -- truth that has assignment')
         print('Blue   -- detection that has assignment')
         print('Cyan   -- detections overlapping with something else')
         print('Yellow -- false negative')
         print('Red    -- false positive')
         if (DM is not None) and (calib is None):
             calib = DM.get_calibration(self.idx)

         # ----- Show result in some projection
         if not isinstance(projection, list):
             projection = [projection]
         for proj in projection:
             if proj == 'bev':
                 """Takes 3D result and projects into BEV"""
                 assert(((lidar is not None) and (calib is not None)) or (DM is not None))
                 if (DM is not None) and (lidar is None):
                     lidar = DM.get_lidar(self.idx)
                 visualize.show_lidar_bev(lidar, calib=calib, labels=labels_all, label_colors=colors_all)
             elif proj == '2d':
                 """Assume front-view camera result"""
                 assert(((image is not None) and (calib is not None)) or (DM is not None))
                 if (DM is not None) and (image is None):
                     image = DM.get_image(self.idx)
                 visualize.show_image_with_boxes(image, calib=calib, labels=labels_all, label_colors=colors_all, show3d=False, inline=True)
             elif proj == 'fv':
                 """Takes 3D result and projects into FV"""
                 raise NotImplementedError
             else:
                 raise NotImplementedError


def associate_detections_truths(detections, truths, metric='3D_IoU', radius=None):
    """
    Determine associations, false positives, and false negatives

    Figure out which detections are associated to which truths,
    which detections are false alarms,
    and which truths are false negatives

    INPUTS:
    detections --> a list of label classes for detection
    truths     --> a list of label classes for truths

    OUTPUT:
    assignments --> list of tuples of assignments (detection --> truth)
    idx_FP      --> indices in the detection list of false positives
    idx_FN      --> indices in the truth list of false negatives
    """
    from avutils.tracking.assignment import single_frame_assign
    # from estimators.tracking.assignment import single_frame_assign

    tol = 1e-8

    # Handle case of none
    no_dets = (detections is None) or (len(detections) == 0)
    no_truths = (truths is None) or (len(truths) == 0)

    if no_dets or no_truths:
        A = np.zeros((0,0))
        assignments = []
        if no_dets:
            idx_FP = []
        else:
            idx_FP = list(range(len(detections)))
        if no_truths:
            idx_FN = []
        else:
            idx_FN = list(range(len(truths)))
    else:
        # Build assignment matrix
        A = _build_A_matrix(detections, truths, metric, radius)

        # Run assignment algorithm
        assignments, idx_FP, idx_FN = single_frame_assign(A,
                                algorithm='hungarian', all_assigned=False)

    return assignments, np.asarray(idx_FP), np.asarray(idx_FN), A


def _build_A_matrix(detections, truths, metric, radius):
    if radius is not None:
        radius2 = radius**2
    else:
        radius2 = None

    # Build assignment matrix
    A = np.zeros((len(detections), len(truths)))
    # Add IoU's to the assignment matrix
    for i, det in enumerate(detections):
        for j, tru in enumerate(truths):
            A[i,j] = _get_assignment_cost(det, tru, metric, radius2)
    return A


def _get_assignment_cost(det, tru, metric, radius2):
    """
    Define the assignment costs when running detection -- truth assignment
    """
    if metric == '3D_IoU':
        corners1 = det.box3d.corners
        corners2 = tru.box3d.corners
        cost = -IOU_3d(corners1, corners2)
        if cost > -1e-8:
            cost = np.inf
    elif metric == 'center_dist':
        center1 = det.box3d.center
        center2 = tru.box3d.center
        cost = np.sum((center1 - center2)**2)
        if (radius2 is not None) and (cost > radius2):
            cost = np.inf
    elif metric == '2D_IoU':
        corners1 = det.box2d.corners
        corners2 = tru.box2d.corners
        cost = -IOU_2d(corners1, corners2)
        if cost > -1e-8:
            cost = np.inf
    elif metric == '2D_FV_IoU':
        # project 3d box into 2D FV
        raise NotImplementedError
    elif metric == '2D_BEV_IoU':
        # project 3d box into 2D BEV
        raise NotImplementedError
    else:
        raise RuntimeError('Cannot understand assignment metric...choices are: [3D_IoU, center_dist, 2D_IoU, 2D_FV_IoU, 2D_BEV_IoU]')
    return cost


def IOU_2d(corners1, corners2):
    """Compute the IoU 2D

    corners1, corners2 - an array of [xmin, ymin, xmax, ymax]
    """
    inter = bbox.box_intersection(corners1, corners2)
    union = bbox.box_union(corners1, corners2)
    return inter / union


def IOU_3d(corners1, corners2):
    """Compute the IoU 3D

    corners1: numpy array (8,3), assume up direction is negative Y
    """
    inter = bbox.box_intersection(corners1, corners2)
    union = bbox.box_union(corners1, corners2)
    return inter / union


# ============================================================
# EVALUATION AND STATS
# ============================================================

def analyze_detection_results(dr_dict, only_2d=False, only_3d=False):
    """
    Perform analysis on a dictionary of detection results
    """
    print('PERFORMING ANALYSIS ON DETECTION RESULTS\n')

    # Get list of objects
    nTruths_all = 0
    nDets_all = 0
    truths_all = []
    assigned_all = []
    fp_all = []
    fn_all = []
    print('---getting labels and categories---')
    for dres in dr_dict.values():
        truths_all.extend(dres.get_objects_of('truths'))
        assigned_all.extend(dres.get_objects_of('assigned'))
        fp_all.extend(dres.get_objects_of('false_positives'))
        fn_all.extend(dres.get_objects_of('false_negatives'))
        nTruths_all += dres.get_number_of('truths')
        nDets_all += dres.get_number_of('detections')

    # Get categories of truths
    label_cat_truths = np.asarray([lab.get_difficulty() for lab in truths_all])
    print('NUMBER OF TRUTH CATEGORIES:')
    for c in np.unique(label_cat_truths):
        print('%s -- %i' % (c, sum(label_cat_truths == c)))
    print('total -- %i\n' % len(label_cat_truths))
    label_cat_fns = np.asarray([lab.get_difficulty() for lab in fn_all])

    print('---making threshold plots---')
    # CDF OF DETECTION ASSIGNMENT PERFORMANCE
    if not only_2d:
        _pct_assigned_by_iou(assigned_all, nTruths_all, iou='3D')
    if not only_3d:
        _pct_assigned_by_iou(assigned_all, nTruths_all, iou='2D')

    # ANALYSIS OF DISTRIBUTIONS
    print('---plotting performance distributions---')
    _label_distribution_analysis(fp_all, id_string='false positives', total_num=nDets_all)
    _label_distribution_analysis(fn_all, id_string='false_negatives', total_num=nTruths_all)

    # ANALYSIS OF DISTRIBUCTIONS BY DIFFICULTY
    _analysis_by_category(_label_distribution_analysis, fn_all, label_cat_fns, id_string='false_negatives')


def _analysis_by_category(func, label_list, label_cat, **kwargs):
    array_lab = np.asarray(label_list)
    if 'id_string' in kwargs:
        id_string_base = kwargs['id_string'] + '--Category {}'

    for cat in np.unique(label_cat):
        idx = np.where(label_cat == cat)[0]
        label_sub = array_lab[idx]
        if 'id_string' in kwargs:
            kwargs['id_string'] = id_string_base.format(cat)
        func(label_sub, **kwargs)


def _label_distribution_analysis(label_list, id_string, total_num=None):
    # -----------
    # INCIDENCE VS DISTANCE
    # -----------
    pr_str = '\nTOTAL NUMBER FOR BELOW PLOT: {}'
    if total_num is not None:
        pr_str = pr_str.format('%i / %i' % (len(label_list), total_num))
    else:
        pr_str = pr_str.format('%i' % len(label_list))

    print(pr_str)

    distance_of_incidence = [lab.get_range() for lab in label_list]
    plt.hist(distance_of_incidence)
    plt.xlabel('Range (m)')
    plt.ylabel('Counts')
    plt.title('{} -- Distance of Incidence'.format(id_string))
    plt.show()


def _pct_assigned_by_iou(assigned_all, nTruths_all, iou='3D'):
    """Make pct assigned curve from a set of assigned objects"""
    # Get IOU
    if iou == '3D':
        ious = np.asarray([IOU_3d(b1.box3d.corners, b2.box3d.corners) for (b1, b2) in assigned_all])
    else:
        ious = np.asarray([IOU_2d(b1.box2d.corners, b2.box2d.corners) for (b1, b2) in assigned_all])

    # Make CDF values
    x = np.sort(ious)[::-1]
    y = np.arange(1, len(x)+1, 1) / nTruths_all

    # Make plot
    plt.plot(x, y, linewidth=3)
    plt.xlabel('IoU Threshold')
    plt.ylabel('CDF of All Truths')
    plt.title('Fraction of Truths with Assignment For %s IoU Threshold' % iou)
    plt.xlim([1.01,0])
    plt.ylim([0,1.01])
    plt.show()


# ============================================================
# FILE/FOLDER BASED OPERATIONS
# ============================================================

def get_detection_results_from_folder(DM, result_path, metric, idxs=None, whitelist_types=['Car'], run_tqdm=True, multiprocess=True):
    """
    Build up a list of detection results from a folder

    pass in idxs for specific frames
    """
    print('Making list of detection results from:\n%s' % result_path)

    # Check directory
    glob_dir = glob.glob(os.path.join(result_path, "*.txt"))
    glob_dir = sorted(glob_dir)

    if not os.path.exists(result_path):
        raise RuntimeError('Cannot find data directory - %s' % result_path)
    elif len(glob_dir) == 0:
        raise RuntimeError('No detection results to be found')

    # Set up storage
    results_all = {}
    idx_list = DM.idxs
    conf_mat = np.zeros((2,2,0))

    # Get indices by looping
    idxs_available = []
    for f in glob_dir:
        if 'log' in f:
            continue
        idx = int(f.split('/')[-1].replace('.txt',''))
        if idxs is not None:
            try:
                iterator = iter(idx)
            except TypeError:
                # not iterable
                if not (idx == idxs):
                    continue
            else:
                # iterable
                if idx not in idxs:
                    continue
        if idx not in idxs_available:
            idxs_available.append(idx)

    # Run analysis on indices
    pool = Pool(min(int(cpu_count()/2), 16))
    part_func = partial(_run_analysis, DM, metric, result_path, whitelist_types)
    # res = pool.map(part_func, idxs_available)
    res = tqdm(pool.imap(part_func, idxs_available), total=len(idxs_available))
    pool.close()
    pool.join()

    # Stitch back together
    for r, idx_avail in zip(res, idxs_available):
        assert r[0].idx == idx_avail
        results_all[r[0].idx] = r[0]
        conf_mat = np.concatenate((conf_mat, r[1]), axis=2)

    return results_all, conf_mat


# Define the function to pool over
def _run_analysis(DM, metric, result_path, whitelist_types, idx):
    # Get truths
    truths_all = DM.get_labels(idx=idx, whitelist_types='all', ignore_types=[])
    truths_whitelist = DM.get_labels(idx=idx, whitelist_types=whitelist_types)
    truths_dontcare = np.asarray([tru for tru in truths_all if tru not in truths_whitelist])
    # Get detections
    lab_file_path = os.path.join(result_path, DM.idx_to_prefix(idx)+'.txt')
    detections = DM.get_labels_from_file(lab_file_path, whitelist_types=whitelist_types)
    # Get results
    DR = DetectionResult(detections, truths_whitelist, idx, metric=metric,
        truths_dontcare=truths_dontcare)
    # Any false positive doesn't count if it lines up with something else
    conf_new = DR.get_confusion()[:,:,None]
    return DR, conf_new
