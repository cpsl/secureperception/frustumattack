# @Author: Spencer Hallyburton <spencer>
# @Date:   2021-02-15
# @Filename: visualize.py
# @Last modified by:   spencer
# @Last modified time: 2021-08-09

import os
import sys

import numpy as np
import cv2
from PIL import Image
from copy import copy, deepcopy

import avutils.perception.maskfilters as maskfilters
import avutils.perception.bbox as bbox

import matplotlib.pyplot as plt

lidar_cmap = plt.cm.get_cmap('hsv', 256)
lidar_cmap = np.array([lidar_cmap(i) for i in range(256)])[:,:3]*255


def get_lidar_color(value, mode='depth'):
    # Min range = 0 --> 1
    # Max range = 100 --> 255
    if mode == 'depth':
        scaling = 100
    elif mode == 'confidence':
        scaling = 2

    idx = max(1, min(255, 255 * value/scaling))
    color = lidar_cmap[int(idx), :]
    return color


def draw_projected_box3d(image, qs, color=(255,255,255), thickness=2):
    ''' Draw 3d bounding box in image
        qs: (8,3) array of vertices for the 3d box in following order:
            1 -------- 0
           /|         /|
          2 -------- 3 .
          | |        | |
          . 5 -------- 4
          |/         |/
          6 -------- 7
    '''
    qs = qs.astype(np.int32)
    for k in range(0,4):
       # Ref: http://docs.enthought.com/mayavi/mayavi/auto/mlab_helper_functions.html
       i,j=k,(k+1)%4
       # use LINE_AA for opencv3
       cv2.line(image, (qs[i,0],qs[i,1]), (qs[j,0],qs[j,1]), color, thickness, cv2.LINE_AA)

       i,j=k+4,(k+1)%4 + 4
       cv2.line(image, (qs[i,0],qs[i,1]), (qs[j,0],qs[j,1]), color, thickness, cv2.LINE_AA)

       i,j=k,k+4
       cv2.line(image, (qs[i,0],qs[i,1]), (qs[j,0],qs[j,1]), color, thickness, cv2.LINE_AA)
    return image


def show_disparity(disparity, is_depth, extent=None):
    if is_depth:
        img = disparity
    else:
        img = Image.fromarray(disparity)

    plt.figure(figsize=[2*x for x in plt.rcParams["figure.figsize"]])
    plt.imshow(img, extent=extent, cmap='magma')
    plt.show()


def show_image(img, extent=None, inline=False):
    if inline:
        pil_im = Image.fromarray(img)
        plt.figure(figsize=[2*x for x in plt.rcParams["figure.figsize"]])
        plt.imshow(pil_im, extent=extent)
        plt.show()
    else:
        Image.fromarray(img).show()

#
# def show_lidar_with_boxes(pc_velo, labels, calib,
#                           img_fov=False, img_width=None, img_height=None, inline=True):
#     ''' Show all LiDAR points.
#         Draw 3d box in LiDAR point cloud (in velo coord system) '''
#     if 'mlab' not in sys.modules: import mayavi.mlab as mlab
#     from viz_util import draw_lidar_simple, draw_lidar, draw_gt_boxes3d
#     if inline:
#         mlab.init_notebook()
#     if type(labels) != np.ndarray:
#         labels = np.asarray([labels])
#     print(('All point num: ', pc_velo.shape[0]))
#     fig = mlab.figure(figure=None, bgcolor=(0,0,0),
#         fgcolor=None, engine=None, size=(1000, 500))
#     if img_fov:
#         mask_image = filter_points_in_image(pc_velo, [img_height, img_width], calib)
#         pc_velo = pc_velo[mask_image, :]
#         print(('FOV point num: ', pc_velo.shape[0]))
#     draw_lidar(pc_velo, fig=fig)
#
#     for obj in labels:
#         if obj.obj_type=='DontCare':continue
#         # Draw 3d bounding box
#         box3d_pts_3d = bbox.compute_box_3d_corners(obj.box3d, calib.P)
#         box3d_pts_3d_velo = calib.project_rect_to_velo(box3d_pts_3d)
#         # Draw heading arrow
#         ori3d_pts_2d, ori3d_pts_3d = bbox.compute_orientation_3d(obj.box3d, calib.P)
#         ori3d_pts_3d_velo = calib.project_rect_to_velo(ori3d_pts_3d)
#         x1,y1,z1 = ori3d_pts_3d_velo[0,:]
#         x2,y2,z2 = ori3d_pts_3d_velo[1,:]
#         draw_gt_boxes3d([box3d_pts_3d_velo], fig=fig)
#         mlab.plot3d([x1, x2], [y1, y2], [z1,z2], color=(0.5,0.5,0.5),
#             tube_radius=None, line_width=1, figure=fig)
#     mlab.show()


def show_lidar_on_image(pc_velo, img, calib, inline=True, colormethod='depth'):
    ''' Project LiDAR points to image '''
    import matplotlib.pyplot as plt

    img1 = np.copy(img)
    box2d_image = bbox.Box2D([0, 0, img1.shape[1], img1.shape[0]])
    points_in_view_filter = maskfilters.filter_points_in_frustum(pc_velo, box2d_image, calib)
    pc_velo_in_view = pc_velo[points_in_view_filter,:]
    imgfov_pc_velo = pc_velo[points_in_view_filter,0:3]
    imgfov_pc_rect = calib.project_velo_to_rect(imgfov_pc_velo)
    imgfov_pc_image = calib.project_velo_to_image(imgfov_pc_velo)

    for i in range(imgfov_pc_image.shape[0]):
        depth = imgfov_pc_rect[i,2]
        if colormethod == 'depth':
            color = get_lidar_color(depth, mode='depth')
        else:
            color = get_lidar_color(pc_velo_in_view[i,4], mode='confidence')
        cv2.circle(img1, (int(np.round(imgfov_pc_image[i,0])),
            int(np.round(imgfov_pc_image[i,1]))),
            2, color=tuple(color), thickness=-1)
    show_image(img1, inline=inline)


def show_image_with_boxes(img, labels, calib, show3d=True, inline=False, label_colors=None, addbox=[]):
    ''' Show image with 2D bounding boxes '''
    img1 = np.copy(img) # for 2d bbox
    img2 = np.copy(img) # for 3d bbox

    if type(labels) == list:
        labels = np.asarray(labels)
    elif type(labels) != np.ndarray:
        labels = np.asarray([labels])

    for i, obj in enumerate(labels):
        if obj.obj_type=='DontCare':continue
        if label_colors is None:
            col = (0, 255, 0)
        else:
            col = parse_color_string(label_colors[i])

        cv2.rectangle(img1, (int(obj.box2d.xmin),int(obj.box2d.ymin)),
            (int(obj.box2d.xmax),int(obj.box2d.ymax)), col, 2)

        # cv2.rectangle(img1, (int(obj.box2d.xmin),int(obj.box2d.ymin)), \
        #     (int(obj.box2d.xmax),int(obj.box2d.ymax)), col, 2)


        # Get points in image plane from 3D bbox
        corners_3d_in_image = bbox.proj_3d_bbox_to_image_plane(obj.box3d, calib.P)
        img2 = draw_projected_box3d(img2, corners_3d_in_image)
        if addbox:
            cv2.rectangle(img1, (int(addbox[0]), int(addbox[1])), (int(addbox[2]), int(addbox[3])), (255,0,0), 2)

    # Plot results-----------------------
    show_image(img1, inline=inline)
    if show3d:
        show_image(img2, inline=inline)


# ==============================================================================
# BEV Utilities
# ==============================================================================
def show_lidar_bev(point_cloud, extent=None, ground=None, calib=None,
                   labels=None, label_colors='white', filter_in_im=False,
                   inline=True, lines=None, line_colors=None, bev_size=[500, 500],
                   colormethod='depth'):
    """
    Show lidar and the detection results (optional) in BEV

    :point_cloud - lidar in the lidar frame
    """
    import matplotlib.pyplot as plt
    pc2 = np.copy(point_cloud)
    lab2 = deepcopy(labels)

    if type(lab2) == list:
        lab2 = np.asarray(lab2)
    elif type(lab2) != np.ndarray:
        lab2 = np.asarray([lab2])

    # Filter points
    if extent is not None:
        # Filter lidar outside extent
        point_filter = maskfilters.filter_points(point_cloud, extent, ground)
        pc2 = pc2[point_filter,:]

        # Filter labels outside extent
        if (lab2 is not None) and (calib is not None):

            label_filter = maskfilters.filter_labels(lab2, calib, extent)
            lab2 = lab2[label_filter]
            if type(label_colors) in [list, np.ndarray]:
                label_colors = label_colors[label_filter]

    # Filter inside image if calibration passed
    if (calib is not None) and filter_in_im:
        imsize = [375, 1242]
        image_filter = maskfilters.filter_points_in_image(pc2, imsize, calib)
        pc2 = pc2[image_filter,:]

    # Get maxes and mins
    min_range = min(0, np.min(pc2[:,0]))
    max_range = max(min_range+10.0, np.max(pc2[:,0]))
    min_width = np.min(pc2[:,1])
    max_width = max(min_width+2.0, np.max(pc2[:,1]))

    if (lab2 is not None) and (calib is not None):
        for i, lab in enumerate(lab2):
            box3d_pts_3d = bbox.compute_box_3d_corners(lab.box3d)
            box3d_pts_3d_velo = calib.project_rect_to_velo(box3d_pts_3d)
            # Update domain based on lab2
            min_range = min(min_range, min(box3d_pts_3d_velo[:,0]) - 5)
            max_range = max(max_range, max(box3d_pts_3d_velo[:,0]) + 5)
            min_width = min(min_width, min(box3d_pts_3d_velo[:,1]) - 2)
            max_width = max(max_width, max(box3d_pts_3d_velo[:,1]) + 2)

    # define the size of the image and scaling factor
    img1 = 0 * np.ones([bev_size[0], bev_size[1], 3], dtype=np.uint8)
    width_scale = (max_width - min_width) / bev_size[0]
    range_scale = (max_range - min_range) / bev_size[1]
    min_arr = np.array([min_range, min_width])
    sc_arr =  np.array([range_scale, width_scale])
    pc_bev = (pc2[:,[0,1]] - min_arr) / sc_arr

    # Colormap
    depth = np.linalg.norm(pc2[:,[0,1]], axis=1)

    # Make image by adding circles
    for i in range(pc_bev.shape[0]):
        if colormethod == 'depth':
            color = get_lidar_color(depth[i], mode='depth')
        elif colormethod == 'confidence':
            color = get_lidar_color(pc2[i,4], mode='confidence')
        else:
            raise NotImplementedError

        # Place in coordinates
        cv2.circle(img1, (int(np.round(pc_bev[i,0])),
            int(np.round(pc_bev[i,1]))),
            2, color=tuple(color), thickness=-1)

    # Add labels
    if (lab2 is not None) and (calib is not None):
        if type(label_colors) not in [list, np.ndarray]:
            ltmp = np.copy(label_colors)
            label_colors = [ltmp for _ in range(len(lab2))]
        for i, lab in enumerate(lab2):
            lcolor = parse_color_string(label_colors[i])
            box3d_pts_3d = bbox.compute_box_3d_corners(lab.box3d)
            box3d_pts_3d_velo = calib.project_rect_to_velo(box3d_pts_3d)
            box3d_pts_2d = (box3d_pts_3d_velo[:,[0,1]] - min_arr) / sc_arr
            img1 = draw_projected_box3d(img1, box3d_pts_2d, color=lcolor, thickness=2)

    # Add lines to the image if passed in
    if lines is not None:
        def plot_line(img1, line, line_color):
            """Assume line is a 2xn array"""
            color = parse_color_string(line_color)
            for p1, p2 in zip(line[:, :-1].T, line[:, 1:].T):
                p1_sc = tuple([int(p) for p in (p1 - min_arr)/sc_arr])
                p2_sc = tuple([int(p) for p in (p2 - min_arr)/sc_arr])
                cv2.line(img1, p1_sc, p2_sc, color, 5)

        if line_colors is None:
            line_colors = 'white'

        # If line is a list, it is a list of lines which are arrays
        if type(lines) is list:
            if type(line_colors) is not list:
                line_colors = [line_colors for _ in len(lines)]
            for l, lc in zip(lines, line_colors):
                plot_line(img1, l, lc)
        elif type(lines) is np.ndarray:
            plot_line(img1, lines, line_colors)
        else:
            raise RuntimeError('Unknown line type')

    # Make the extent have labels
    viz_extent = [min_range, max_range, min_width, max_width]

    # Flip because image
    img1 = np.flip(img1, axis=0)
    show_image(img1, extent=viz_extent, inline=inline)


def parse_color_string(cstring):
    if cstring == 'white':
        lcolor = (255,255,255)
    elif cstring == 'green':
        lcolor = (0,255,0)
    elif cstring == 'red':
        lcolor = (255,0,0)
    elif cstring == 'blue':
        lcolor = (0,0,255)
    elif cstring == 'cyan':
        lcolor = (0,255,255)
    elif cstring == 'lightblue':
        lcolor = (51, 255, 255)
    elif cstring == 'black':
        lcolor = (0,0,0)
    elif cstring == 'yellow':
        lcolor = (255, 255, 0)
    else:
        raise ValueError('Unknown color type')
    return lcolor
