# @Author: Spencer Hallyburton <spencer>
# @Date:   2021-02-17
# @Filename: maskfilters.py
# @Last modified by:   spencer
# @Last modified time: 2021-08-04
"""
A collection of slicers, masks, and filtering operations for perception data

The code is independent of data source so long as format is standard
"""

import numpy as np
import avutils.perception.bbox as bbox


# ==============================================================================
# FILTERS
# ==============================================================================

def _get_extents_filter(loc_data, extents):
    """
    Returns the extent filter for data of [N x 3] or [N x 4]
    """
    # Filter points within certain xyz range
    x_extents = extents[0]
    y_extents = extents[1]
    z_extents = extents[2]

    extents_filter = (loc_data[:,0] > x_extents[0]) & \
                     (loc_data[:,0] < x_extents[1]) & \
                     (loc_data[:,1] > y_extents[0]) & \
                     (loc_data[:,1] < y_extents[1]) & \
                     (loc_data[:,2] > z_extents[0]) & \
                     (loc_data[:,2] < z_extents[1])

    return extents_filter


def filter_points(point_cloud, extents, ground_plane=None, offset_dist=2.0):
    """
    Creates a point filter using the 3D extents and ground plane
    :param point_cloud: Point cloud in the standard for mof [N, 3] or [N,4]
    :param extents: 3D area in the form
        [[min_x, max_x], [min_y, max_y], [min_z, max_z]]
    :param ground_plane: Optional, coefficients of the ground plane
        (a, b, c, d) - in the lidar frame
    :param offset_dist: If ground_plane is provided, removes points above
        this offset from the ground_plane
    :return: A binary mask for points within the extents and offset plane
    """

    pc2 = np.asarray(point_cloud[:,0:3])

    # Filter points within certain xyz range
    extents_filter = _get_extents_filter(pc2, extents)

    # Add ground plane filter
    if ground_plane is not None:
        ground_plane = np.array(ground_plane)

        # Calculate filter using ground plane
        ones_col = np.ones(pc2.shape[0])
        padded_points = np.hstack([pc2, ones_col[:,None]])

        offset_plane = ground_plane + [0, 0, 0, -offset_dist]

        # Create plane filter
        dot_prod = np.dot(offset_plane, np.transpose(padded_points))
        plane_filter = dot_prod < 0

        # Combine the two filters
        point_filter = np.logical_and(extents_filter, plane_filter)
    else:
        # Only use the extents for filtering
        point_filter = extents_filter

    return point_filter


def filter_labels_range(labels, range_max):
    """
    Return mask for which labels are inside the range
    """
    lab_centers = np.asarray([l.box3d.t for l in labels])
    return np.linalg.norm(lab_centers, axis=1) < range_max


def filter_points_range(point_cloud, range_max):
    """
    Return mask for which points are inside the range
    """
    return np.linalg.norm(point_cloud, axis=1) <= range_max


def filter_labels(labels, calib, extent):
    """
    Filter if the label center is outside the extents

    extent is usually defined in the lidar frame of reference
    """
    lab_centers = np.asarray([l.box3d.t for l in labels])
    # Convert to velo coordinates
    lab_centers_velo = calib.project_rect_to_velo(lab_centers)
    label_filter = _get_extents_filter(lab_centers_velo, extent)
    return label_filter


def filter_points_in_image(point_cloud, im_size, calib):
    """ Filter points based on if they fall within the image specified"""
    # Filter points based on the image coordinates
    point_in_im = calib.project_velo_to_image(point_cloud[:, 0:3])
    print(im_size)
    print(point_in_im)
    point_in_im_mask = (point_in_im[:, 0] > 0) & \
                       (point_in_im[:, 0] < im_size[1]) & \
                       (point_in_im[:, 1] > 0) & \
                       (point_in_im[:, 1] < im_size[0]) & \
                       (point_cloud[:,0] > 0)

    return point_in_im_mask


def filter_points_in_frustum(point_cloud, box2d_image, calib):
    """
    Gets all lidar points in a frustum defined by the 2d box in image space

    point_cloud - velo coordinates
    """
    pc_velo = point_cloud[:,0:3]
    # if point_cloud.shape[1]==4:
    #
    # else:
    #     pc_velo = point_cloud
    lidar_image = calib.project_velo_to_image(pc_velo)
    lidar_mask1 = lidar_image[:,0] > box2d_image.box2d[0]
    lidar_mask2 = lidar_image[:,1] > box2d_image.box2d[1]
    lidar_mask3 = lidar_image[:,0] < box2d_image.box2d[2]
    lidar_mask4 = lidar_image[:,1] < box2d_image.box2d[3]
    frustum_filter = lidar_mask1 & lidar_mask2 & lidar_mask3 & lidar_mask4

    return frustum_filter


def filter_points_in_pillar(point_cloud, box_bev):
    """
    Take a 3d box and expand the vertical axis to +/- infinity to create a pillar

    box_bev is 4x2 and is in m
    box_bev is defined as:

    x - forward
    y - left
    z - up
    """
    # Project point cloud to bev -- z is up so is reduced
    pc_velo_bev = np.delete(point_cloud[:,0:3], 2, axis=1)

    # Get points in hull
    return bbox.in_hull(pc_velo_bev, box_bev)


def filter_points_in_shadow(point_cloud, box2d_image, box3d_label, calib):
    """
    Gets all lidar points in the shadow which is the frustum behind the object
    """

    # Filter within frustum
    frustum_filter = filter_points_in_frustum(point_cloud, box2d_image, calib)

    # Filter by bounding box
    box_bev = box3d_label.get_corners_bev_velo(calib=calib)
    pillar_filter = filter_points_in_pillar(point_cloud, box_bev)

    # Filter by range
    box_center_velo = calib.project_rect_to_velo(box3d_label.t[:,None].T)
    range_filter = filter_points_range(point_cloud, np.linalg.norm(box_center_velo))

    return frustum_filter & (~pillar_filter) & (~range_filter)


def filter_points_slice(point_cloud, area_extents, ground_plane, height_min, height_max):
    """ Creates a slice filter to take a slice of the point cloud between
        ground_offset_dist and offset_dist above the ground plane

    Args:
        point_cloud: Point cloud in the shape (N,3) or (N,4)
        area_extents: 3D area extents
        ground_plane: ground plane coefficients
        offset_dist: max distance above the ground
        ground_offset_dist: min distance above the ground plane

    Returns:
        A boolean mask if shape (N,) where
            True indicates the point should be kept
            False indicates the point should be removed
    """

    # Filter points within certain xyz range and offset from ground plane
    top_to_road = filter_points(point_cloud, area_extents,
                                               ground_plane, height_max)

    # Filter points within 0.2m of the road plane
    low_to_road = filter_points(point_cloud, area_extents,
                                             ground_plane,
                                             height_min)

    slice_filter = np.logical_xor(top_to_road, low_to_road)
    return slice_filter


def filter_points_in_box(points, box_corners, include_boundary=True):
    """
    Returns the points that are inside the box
    points = (N,3)
    box_corners = (8,3)

    returns set of indices of point sets that are inside or on boundary of box

    reference to: https://stackoverflow.com/questions/21037241/
    how-to-determine-a-point-is-inside-or-outside-a-cube#:~:
    text=If%20the%20projection%20lies%20inside,the%20point%20P%20is%20V.
    """
    if points.shape[1]==4:
        pc = points[:,0:3]
    else:
        pc = points
    t1,t2,t3,t4,b1,b2,b3,b4 = box_corners

    dir1 = t1-b1
    size1 = np.linalg.norm(dir1)
    dir1 = dir1 / size1

    dir2 = b2-b1
    size2 = np.linalg.norm(dir2)
    dir2 = dir2 / size2

    dir3 = b4-b1
    size3 = np.linalg.norm(dir3)
    dir3 = dir3 / size3

    box_center = (b1+t3) / 2.0
    dir_vec = pc - box_center

    if include_boundary:
        res1 = (np.absolute(np.dot(dir_vec, dir1)) * 2) <= size1
        res2 = (np.absolute(np.dot(dir_vec, dir2)) * 2) <= size2
        res3 = (np.absolute(np.dot(dir_vec, dir3)) * 2) <= size3
    else:
        res1 = (np.absolute(np.dot(dir_vec, dir1)) * 2) < size1
        res2 = (np.absolute(np.dot(dir_vec, dir2)) * 2) < size2
        res3 = (np.absolute(np.dot(dir_vec, dir3)) * 2) < size3

    box_filter = res1 & res2 & res3
    return box_filter


def filter_points_in_object_bbox(point_cloud, box3d, calib):
    """
    Gets lidar points that fall within a bounding box object
    """
    if point_cloud.shape[1] == 4:
        pc_velo = point_cloud[:,0:3]
    else:
        pc_velo = point_cloud
    # Get points in each bounding box
    box3d_pts_3d = bbox.compute_box_3d_corners(box3d)
    box3d_pts_3d_velo = calib.project_rect_to_velo(box3d_pts_3d)
    box_filter = filter_points_in_box(pc_velo, box3d_pts_3d_velo)

    return box_filter


def filter_objects_in_frustum(labels_1, labels_2, calib):
    # Loop if lists
    if (type(labels_1) is not list) and (type(labels_1) is not np.ndarray):
        labels_1 = [labels_1]
    if (type(labels_2) is not list) and (type(labels_2) is not np.ndarray):
        labels_2 = [labels_2]

    in_frustum = np.zeros((len(labels_1), len(labels_2)), dtype=bool)

    for i in range(len(labels_1)):
        for j in range(len(labels_2)):
            if _item_in_frustum(labels_1[i], labels_2[j], calib) or \
               _item_in_frustum(labels_2[j], labels_1[i], calib):
                in_frustum[i,j] = True

    return in_frustum


def _item_in_frustum(lab1, lab2, calib):
    box2d_1 = lab1.box3d.project_to_2d_bbox(calib=calib)
    box2d_2 = lab2.box3d.project_to_2d_bbox(calib=calib)

    if bbox.box_intersection(box2d_1.box2d, box2d_2.box2d) > 0:
        return True
    else:
        return False
