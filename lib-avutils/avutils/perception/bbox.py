# @Author: Spencer Hallyburton <spencer>
# @Date:   2021-02-04
# @Filename: bbox_util.py
# @Last modified by:   spencer
# @Last modified time: 2021-08-11

import os
import sys

from copy import copy, deepcopy
import numpy as np
# from PIL import Image
from scipy.spatial import ConvexHull

import avutils.tforms as tforms


# ==============================================================================
# Define bounding boxes
# ==============================================================================

class Box2D():
    def __init__(self, box2d):
        self.xmin = box2d[0]
        self.ymin = box2d[1]
        self.xmax = box2d[2]
        self.ymax = box2d[3]
        self.box2d = np.asarray(box2d)
        self.center = np.asarray([(self.xmin+self.xmax)/2, (self.ymin+self.ymax)/2])
        self.corners = np.array([[self.xmin, self.ymin],
                                 [self.xmin, self.ymax],
                                 [self.xmax, self.ymax],
                                 [self.xmax, self.ymin]])

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return 'Box2D=[%.2f, %.2f, %.2f, %.2f]' % (self.xmin, self.ymin, self.xmax, self.ymax)

    def __eq__(self, other):
        return np.all(self.box2d == other.box2d)


class Box3D():
    def __init__(self, box3d):
        self.h, self.w, self.l = box3d[0:3]
        self.t = box3d[3].flatten()  # (x,y,z) in camera coordindates
        self.ry = box3d[4]
        self.corners = compute_box_3d_corners(box3d=self)

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return 'Box3D=[h: %.2f, w: %.2f, l: %.2f] x (x: %.2f y: %.2f, z: %.2f)\n  ry: %.2f\n' % (self.h, self.w, self.l, self.t[0], self.t[1], self.t[2], self.ry)

    def __eq__(self, other):
        return np.all(self.corners == other.corners)

    @property
    def center(self):
        return self.t

    @property
    def heading(self):
        return self.ry

    def get_corners_bev_rect(self, calib=None, P=None):
        corners_rect = self.corners
        return calib.project_rect_to_bev(corners_rect)

    def get_corners_bev_velo(self, calib=None, P=None):
        corners_rect = self.corners
        return calib.project_velo_to_bev(calib.project_rect_to_velo(corners_rect))

    def project_to_2d_bbox(self, calib=None, P=None):
        """Project 3D bounding box into a 2D bounding box"""
        if (calib is None) and (P is None):
            raise ValueError('Have to set either calib or P')
        elif calib is not None:
            return proj_3d_bbox_to_2d_bbox(self, calib.P)
        else:
            return proj_3d_bbox_to_2d_bbox(self, P)

    def project_corners_to_2d_image_plane(self, calib=None, P=None):
        """Project 3D bounding box corners only image plane"""
        if (calib is None) and (P is None):
            raise ValueError('Have to set either calib or P')
        elif calib is not None:
            return proj_3d_bbox_to_image_plane(self, calib.P)
        else:
            return proj_3d_bbox_to_image_plane(self, P)


def make_boxes_from_bev_corners(bev_corners_velo, calib, height_center=-1.5, height=1.52):
    """
    Take BEV corners and assume things to make a 3D box

    bev_corners are in velo frame
    """

    height_center = np.array([height_center])
    box_center_velo = np.concatenate((np.mean(bev_corners_velo, axis=0), height_center))

    side_1 = np.linalg.norm(bev_corners_velo[0,:] - bev_corners_velo[1,:])
    side_2 = np.linalg.norm(bev_corners_velo[0,:] - bev_corners_velo[2,:])
    box_yaw = tforms.get_yaw_from_bev_corners(bev_corners_velo)

    # Project into 2d coordinates
    box_center_rect = calib.project_velo_to_rect(box_center_velo[:,None].T)
    box_detail = [height, min(side_1, side_2), max(side_1, side_2), box_center_rect, box_yaw]
    box3d = Box3D(box_detail)
    box2d = box3d.project_to_2d_bbox(calib=calib)

    return box3d, box2d

# ==============================================================================
# Other Utilities
# ==============================================================================


def polygon_clip(subjectPolygon, clipPolygon):
   """ Clip a polygon with another polygon.
   Ref: https://rosettacode.org/wiki/Sutherland-Hodgman_polygon_clipping#Python
   Args:
     subjectPolygon: a list of (x,y) 2d points, any polygon.
     clipPolygon: a list of (x,y) 2d points, has to be *convex*
   Note:
     **points have to be counter-clockwise ordered**
   Return:
     a list of (x,y) vertex point for the intersection polygon.
   """
   def inside(p):
      return(cp2[0]-cp1[0])*(p[1]-cp1[1]) > (cp2[1]-cp1[1])*(p[0]-cp1[0])

   def computeIntersection():
      dc = [ cp1[0] - cp2[0], cp1[1] - cp2[1] ]
      dp = [ s[0] - e[0], s[1] - e[1] ]
      n1 = cp1[0] * cp2[1] - cp1[1] * cp2[0]
      n2 = s[0] * e[1] - s[1] * e[0]
      n3 = 1.0 / (dc[0] * dp[1] - dc[1] * dp[0])
      return [(n1*dp[0] - n2*dc[0]) * n3, (n1*dp[1] - n2*dc[1]) * n3]

   outputList = subjectPolygon
   cp1 = clipPolygon[-1]
   for clipVertex in clipPolygon:
      cp2 = clipVertex
      inputList = outputList
      outputList = []
      s = inputList[-1]

      for subjectVertex in inputList:
         e = subjectVertex
         if inside(e):
            if not inside(s):
               outputList.append(computeIntersection())
            outputList.append(e)
         elif inside(s):
            outputList.append(computeIntersection())
         s = e
      cp1 = cp2
      if len(outputList) == 0:
          return None
   return(outputList)


def poly_area(x,y):
    """ Ref: http://stackoverflow.com/questions/24467972/calculate-area-of-polygon-given-x-y-coordinates """
    return 0.5*np.abs(np.dot(x,np.roll(y,1))-np.dot(y,np.roll(x,1)))


def convex_hull_intersection(p1, p2):
    """ Compute area of two convex hull's intersection area.
        p1,p2 are a list of (x,y) tuples of hull vertices.
        return a list of (x,y) for the intersection and its volume
    """
    inter_p = polygon_clip(p1,p2)
    if inter_p is not None:
        hull_inter = ConvexHull(inter_p)
        return inter_p, hull_inter.volume
    else:
        return None, 0.0


def box_area(corners):
    """Compute 2D box area
    corners - an array of [xmin, ymin, xmax, ymax]
    """
    return (corners[2] - corners[0] + 1) * (corners[3] - corners[1] + 1)


def box_volume(corners):
    """Compute 3D box volume
    corners1: numpy array (8,3), assume up direction is negative Y
    """
    a = np.sqrt(np.sum((corners[0,:] - corners[1,:])**2))
    b = np.sqrt(np.sum((corners[1,:] - corners[2,:])**2))
    c = np.sqrt(np.sum((corners[0,:] - corners[4,:])**2))
    return a*b*c


def _box_intersection_2d(corners1, corners2):
    """Compute intersection of 2D boxes"""
    xA = max(corners1[0], corners2[0])
    yA = max(corners1[1], corners2[1])
    xB = min(corners1[2], corners2[2])
    yB = min(corners1[3], corners2[3])
    return max(0, xB - xA + 1) * max(0, yB - yA + 1)


def _box_intersection_3d(corners1, corners2):
    ''' Compute 3D bounding box IoU.

    From: https://github.com/AlienCat-K/3D-IoU-Python/blob/master/3D-IoU-Python.py
    Input:
        corners1: numpy array (8,3), assume up direction is negative Y
        corners2: numpy array (8,3), assume up direction is negative Y
    Output:
        iou: 3D bounding box IoU
        iou_2d: bird's eye view 2D bounding box IoU
    todo (kent): add more description on corner points' orders.
    '''
    if np.any(np.isnan(corners1)) or np.any(np.isnan(corners2)):
        print(corners1)
        print(corners2)
        raise RuntimeError('NaN somewhere!')

    # Add some very small noise
    noise = 1e-12*np.linalg.norm(corners2[0,:] - corners2[1,:])
    corners2 = copy(corners2) + noise

    # corner points are in counter clockwise order
    rect1 = [(corners1[i,0], corners1[i,2]) for i in range(3,-1,-1)]
    rect2 = [(corners2[i,0], corners2[i,2]) for i in range(3,-1,-1)]
    area1 = poly_area(np.array(rect1)[:,0], np.array(rect1)[:,1])
    area2 = poly_area(np.array(rect2)[:,0], np.array(rect2)[:,1])
    if np.all(rect1 == rect2):
        inter = None
        inter_area = box_volume(corners1)
    else:
        inter, inter_area = convex_hull_intersection(rect1, rect2)
    iou_2d_bev = inter_area/(area1+area2-inter_area)
    ymax = min(corners1[0,1], corners2[0,1])
    ymin = max(corners1[4,1], corners2[4,1])
    return inter_area * max(0.0, ymax-ymin)


def box_intersection(corners1, corners2):
    if len(corners1) == 4:
        return _box_intersection_2d(corners1, corners2)
    else:
        return _box_intersection_3d(corners1, corners2)


def _box_union_2d(corners1, corners2):
    return box_area(corners1) + box_area(corners2) - box_intersection(corners1, corners2)


def _box_union_3d(corners1, corners2):
    return box_volume(corners1) + box_volume(corners2) - box_intersection(corners1, corners2)


def box_union(corners1, corners2):
    if len(corners1) == 4:
        return _box_union_2d(corners1, corners2)
    else:
        return _box_union_3d(corners1, corners2)


def compute_box_size(corners_3d, heading_angle):
    """
    Takes an object's set of corners and gets h, w, l

    Corners are specified in the image rectangular coordinates
    Assumes corner convention in order to specify which is which
    """
    R = tforms.roty(heading_angle)
    box_oriented = corners_3d @ R  # invert the rotation
    l = max(box_oriented[:,0]) - min(box_oriented[:,0])
    h = max(box_oriented[:,1]) - min(box_oriented[:,1])
    w = max(box_oriented[:,2]) - min(box_oriented[:,2])
    return (l,w,h)


def compute_box_3d_corners(box3d):
    """
    Computes the 3d coordinates of the bounding box

    Returns coordiantes in the rectangular image coordinates (same as box center
    coordinates)
    """
    # Get rotation about yaw axis
    R = tforms.roty(box3d.ry)

    # Get 3D coordinates of a nominal box (origin at center)
    # X right, Y down, Z forward
    l = box3d.l
    w = box3d.w
    h = box3d.h
    x_corners = [l/2,l/2,-l/2,-l/2,l/2,l/2,-l/2,-l/2]
    y_corners = [0,0,0,0,-h,-h,-h,-h]
    # y_corners = [h/2,h/2,h/2,h/2,-h/2,-h/2,-h/2,-h/2]
    z_corners = [w/2,-w/2,-w/2,w/2,w/2,-w/2,-w/2,w/2]
    # x_corners = [w/2,-w/2,-w/2,w/2,w/2,-w/2,-w/2,w/2]
    # y_corners = [h/2,h/2,h/2,h/2,-h/2,-h/2,-h/2,-h/2]
    # z_corners = [l/2,l/2,-l/2,-l/2,l/2,l/2,-l/2,-l/2]

    # rotate and translate 3d bounding box
    corners_3d = np.dot(R, np.vstack([x_corners,y_corners,z_corners]))
    corners_3d[0,:] = corners_3d[0,:] + box3d.t[0];
    corners_3d[1,:] = corners_3d[1,:] + box3d.t[1];
    corners_3d[2,:] = corners_3d[2,:] + box3d.t[2];
    corners_3d = np.transpose(corners_3d)

    return corners_3d


def compute_orientation_3d(box3d, P):
    ''' Takes an object and a projection matrix (P) and projects the 3d
        object orientation vector into the image plane.
        Returns:
            orientation_2d: (2,2) array in left image coord.
            orientation_3d: (2,3) array in in rect camera coord.
    '''

    # compute rotational matrix around yaw axis
    R = tforms.roty(box3d.ry)

    # orientation in object coordinate system
    orientation_3d = np.array([[0.0, box3d.l],[0,0],[0,0]])

    # rotate and translate in camera coordinate system, project in image
    orientation_3d = np.dot(R, orientation_3d)
    orientation_3d[0,:] = orientation_3d[0,:] + box3d.t[0]
    orientation_3d[1,:] = orientation_3d[1,:] + box3d.t[1]
    orientation_3d[2,:] = orientation_3d[2,:] + box3d.t[2]

    # vector behind image plane?
    if np.any(orientation_3d[2,:]<0.1):
      orientation_2d = None
      return orientation_2d, np.transpose(orientation_3d)

    # project orientation into the image plane
    orientation_2d = tforms.project_to_image(np.transpose(orientation_3d), P);
    return orientation_2d, np.transpose(orientation_3d)


def in_hull(p, hull):
    from scipy.spatial import Delaunay
    if not isinstance(hull,Delaunay):
        hull = Delaunay(hull)
    return hull.find_simplex(p)>=0


def proj_3d_bbox_to_image_plane(box3d, P):
    # Get 3D box points
    box3d_pts_3d = compute_box_3d_corners(box3d)

    # Project into image plane
    corners_3d_in_image = tforms.project_to_image(box3d_pts_3d, P)
    return corners_3d_in_image


def proj_3d_bbox_to_2d_bbox(box3d, P):
    # Get 3D box points
    corners_3d_in_image = proj_3d_bbox_to_image_plane(box3d, P)
    # Get mins and maxes to make 2D bbox
    xmin, xmax = np.min(corners_3d_in_image[:,0]), np.max(corners_3d_in_image[:,0])
    ymin, ymax = np.min(corners_3d_in_image[:,1]), np.max(corners_3d_in_image[:,1])
    box2d = np.array([xmin, ymin, xmax, ymax])
    return Box2D(box2d)


def validate_points_in_region(calib, label, image, pc_velo):
    raise NotImplementedError

    # Image shape
    img_height, img_width, img_channel = image.shape

    # PC
    pc_rect = np.zeros_like(pc_velo)
    pc_rect[:,0:3] = calib.project_velo_to_rect(pc_velo[:,0:3])
    pc_rect[:,3] = pc_velo[:,3]

    # 2D BOX: Get pts rect backprojected
    box2d = label.box2d
    augmentX = 1
    perturb_box2d = False

    # mask_image = filter_points_in_image(pc_velo, im_size, calib)

    _, pc_image_coord, img_fov_inds = get_lidar_in_image_fov(pc_velo[:,0:3],
        calib, 0, 0, img_width, img_height, True)


    for _ in range(augmentX):
        # Augment data by box2d perturbation
        if perturb_box2d:
            xmin,ymin,xmax,ymax = random_shift_box2d(box2d)
            print(box2d)
            print(xmin,ymin,xmax,ymax)
        else:
            xmin,ymin,xmax,ymax = box2d
        box_fov_inds = (pc_image_coord[:,0]<xmax) & \
            (pc_image_coord[:,0]>=xmin) & \
            (pc_image_coord[:,1]<ymax) & \
            (pc_image_coord[:,1]>=ymin)
        box_fov_inds = box_fov_inds & img_fov_inds

    # Point cloud
    pc_in_box_fov = pc_rect[box_fov_inds,:]

    # Get indices
    box3d_pts_3d = compute_box_3d_corners(label.box3d)
    _, inds = extract_pc_in_box3d(pc_in_box_fov, box3d_pts_3d)

    # Print
    print('{} / {}'.format(sum(inds), len(inds)))
    if sum(inds) == 0:
        raise
