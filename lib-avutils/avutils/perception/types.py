

import os
import numpy as np

from avutils import tforms
from avutils.perception import bbox
from avutils.perception import maskfilters

# ==============================================================================
# Label Managers
# ==============================================================================

class ObjectLabel():
    def __init__(self, source, obj_type, box2d, box3d):
        """
        Initialization of the object label.

        obj_type  - 'Car', 'Pedestrian', 'Cyclist'
        box2d  - [xmin, ymin, xmax, ymax] for the 2d bounding box in image coords.
        box3d  - [h, w, l, (x,y,z), ry] for the 3d bounding box. (x,y,z) in rect. camera coords.
        """
        self.source = source
        self.obj_type = obj_type
        self.box2d = bbox.Box2D(box2d)
        self.box3d = bbox.Box3D(box3d)
        self.range = np.linalg.norm(self.box3d.t)

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return '%s Object3DLabel:\n  Type=%s\n %s\n  %s' % (self.source, self.obj_type, str(self.box2d), str(self.box3d))

    def __eq__(self, other):
        c1 = self.box2d == other.box2d
        c2 = self.box3d == other.box3d
        return c1 and c2

    def get_difficulty():
        raise NotImplementedError

    def view_label(self, image, calib):
        visualize.show_image_with_boxes(image, [self], calib, show3d=True, inline=True, addbox=[])


class KittiObject3DLabel(ObjectLabel):
    def __init__(self, label_file_line):
        """
        Kitti Object Label

        Data indices:
        0  - object type
        1  - truncation
        2  - occlusion
        3  - alpha (observation angle)
        4  - xmin for 2d box
        5  - ymin for 2d box
        6  - xmax for 2d box
        7  - ymax for 2d box
        8  - h for 3d box
        9  - w for 3d box
        10 - l for 3d box
        11 - x for 3d box loc in camera rect coords
        12 - y for 3d box loc in camera rect coords
        13 - z for 3d box loc in camera rect coords
        14 - ry for 3d box (heading angle)
        """
        data = label_file_line.strip('\n').split(' ')
        data[1:] = [float(x) for x in data[1:]]
        # KITTI data indices:
        # Get the box info
        obj_type = data[0]  # 'Car', 'Pedestrian', ...
        box2d = np.array([data[4], data[5], data[6], data[7]])
        t = np.array([data[11],data[12],data[13]])
        box3d = [data[8], data[9], data[10], t, data[14]]

        # Init the super
        super().__init__('KITTI', obj_type, box2d, box3d)

        # Kitti-Specific things
        self.truncation = data[1] # truncated pixel ratio [0..1]
        self.occlusion = int(data[2]) # 0=visible, 1=partly occluded, 2=fully occluded, 3=unknown
        self.alpha = data[3] # object observation angle [-pi..pi]

    def write_text(self):
        '''Outputs a list of information to write itself to a file externally'''
        return get_kitti_label_text_from_bbox(self.obj_type, self.box3d, self.box2d,
                                              trunc=self.truncation,
                                              occ=self.occlusion,
                                              alpha=self.alpha)

    def get_difficulty(self):
        """
        Difficulty index:
        - 0 = easy
        - 1 = moderate
        - 2 = hard
        - 3 = not specified/impossible??
        """
        if (label.occlusion < 0) or (label.truncation < 0):
            raise RuntimeError('Truth fields must be filled for difficulty')

        if label.source.lower() == 'kitti':
            bbox_h = label.box2d.get_corners()[3] - label.box2d.get_corners()[1]
            occ = label.occlusion
            trunc = label.truncation

            # Easy cases
            if (bbox_h >= 40) and (occ == 0) and (trunc <= .15):
                diff = 'easy'
            elif (bbox_h >= 25) and (occ <= 1) and (trunc <= .30):
                diff = 'moderate'
            elif (bbox_h >= 25) and (occ <= 2) and (trunc <= .50):
                diff = 'hard'
            else:
                diff = 'undetermined'
        else:
            raise NotImplementedError
        return diff


def get_kitti_label_text_from_bbox(obj_type, box3d, box2d, trunc=-1, occ=-1, alpha=-1):
    return get_kitti_label_text(obj_type=obj_type,
                                truncation=trunc,
                                occlusion=occ,
                                alpha=alpha,
                                xmin=box2d.xmin,
                                ymin=box2d.ymin,
                                xmax=box2d.xmax,
                                ymax=box2d.ymax,
                                h=box3d.h,
                                w=box3d.w,
                                l=box3d.l,
                                tx=box3d.t[0],
                                ty=box3d.t[1],
                                tz=box3d.t[2],
                                ry=box3d.ry)


def get_kitti_label_text(obj_type, truncation, occlusion, alpha, xmin, ymin, xmax, ymax, h, w, l, tx, ty, tz, ry):
    return "{obj_type:s} {truncation:.2f} {occlusion:.4f} {alpha:.2f} {xmin:.2f} {ymin:.2f} {xmax:.2f} {ymax:.2f} {h:.2f} {w:.2f} {l:.2f} {tx:.2f} {ty:.2f} {tz:.2f} {ry:.2f}".format(obj_type=obj_type, truncation=truncation, occlusion=occlusion, alpha=alpha, xmin=xmin, ymin=ymin, xmax=xmax, ymax=ymax, h=h, w=w, l=l, tx=tx, ty=ty, tz=tz, ry=ry)




# ==============================================================================
# Calibration Managers
# ==============================================================================
class Calibration():
    """ Calibration matrices and utils
        3d XYZ in <label>.txt are in rect camera coord.
        2d box xy are in image2 coord
        Points in <lidar>.bin are in Velodyne coord.

        y_image2 = P^2_rect * x_rect
        y_image2 = P^2_rect * R0_rect * Tr_velo_to_cam * x_velo
        x_ref = Tr_velo_to_cam * x_velo
        x_rect = R0_rect * x_ref

        P^2_rect = [f^2_u,  0,      c^2_u,  -f^2_u b^2_x;
                    0,      f^2_v,  c^2_v,  -f^2_v b^2_y;
                    0,      0,      1,      0]
                 = K * [1|t]

        image2 coord:
         ----> x-axis (u)
        |
        |
        v y-axis (v)

        velodyne coord:
        front x, left y, up z

        rect/ref camera coord:
        right x, down y, front z

        Ref (KITTI paper): http://www.cvlibs.net/publications/Geiger2013IJRR.pdf

        TODO(rqi): do matrix multiplication only once for each projection.
    """
    def __init__(self, V2C, C2V, R0, P):
        """
        Base class initialization

        R0 -
        C2V -
        V2C -
        P - Camera intrinsics matrix
        """
        self.V2C = V2C
        self.C2V = C2V
        self.R0 = R0
        self.P = P
        # Camera intrinsics and extrinsics
        self.c_u = self.P[0,2]
        self.c_v = self.P[1,2]
        self.f_u = self.P[0,0]
        self.f_v = self.P[1,1]
        self.b_x = self.P[0,3]/(-self.f_u) # relative
        self.b_y = self.P[1,3]/(-self.f_v)

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return 'Calibration Class:\nV2C:\n%s\n\nP:\n%s' % (str(self.V2C), str(self.P))

    # ===========================
    # ------- 3d to 3d ----------
    # ===========================
    def project_velo_to_ref(self, pts_3d_velo):
        """ Projects lidar coordinates to ref

        pts_3d_velo - nx3 or nx4 where 4th channel is intensity
        """
        pts_3d_velo_hom = tforms.cart2hom(pts_3d_velo[:,0:3]) # nx4
        pts_3d_ref = np.dot(pts_3d_velo_hom, np.transpose(self.V2C))
        if pts_3d_velo.shape[1] == 4:
            pts_3d_ref = np.hstack([pts_3d_ref, pts_3d_velo[:,3][:,None]])
        return pts_3d_ref

    def project_ref_to_velo(self, pts_3d_ref):
        """ Projects ref to back to lidar

        pts_3d_ref - nx3 or nx4 where 4th channel is intensity
        """
        pts_3d_ref_hom = tforms.cart2hom(pts_3d_ref[:,0:3]) # nx4
        pts_3d_velo = np.dot(pts_3d_ref_hom, np.transpose(self.C2V))
        if pts_3d_ref.shape[1] == 4:
            pts_3d_velo = np.hstack([pts_3d_velo, pts_3d_ref[:,3][:,None]])
        return pts_3d_velo

    def project_rect_to_ref(self, pts_3d_rect):
        """ Projects camera rectangular to ref

        pts_3d_rect - nx3 or nx4 where 4th channel is intensity
        """
        pts_3d_ref = np.transpose(np.dot(np.linalg.inv(self.R0), np.transpose(pts_3d_rect[:,0:3])))
        if pts_3d_rect.shape[1] == 4:
            pts_3d_ref = np.hstack([pts_3d_ref, pts_3d_rect[:,3][:,None]])
        return pts_3d_ref

    def project_ref_to_rect(self, pts_3d_ref):
        """ Projects ref to camera rectangular

        pts_3d_rect - nx3 or nx4 where 4th channel is intensity
        """
        pts_3d_rect = np.transpose(np.dot(self.R0, np.transpose(pts_3d_ref[:,0:3])))
        if pts_3d_ref.shape[1] == 4:
            pts_3d_rect = np.hstack([pts_3d_rect, pts_3d_ref[:,3][:,None]])
        return pts_3d_rect

    def project_rect_to_velo(self, pts_3d_rect):
        """ Projects rect to velo

        pts_3d_rect - nx3 or nx4 where 4th channel is intensity
        """
        pts_3d_ref = self.project_rect_to_ref(pts_3d_rect[:,0:3])
        pts_3d_velo = self.project_ref_to_velo(pts_3d_ref)
        if pts_3d_rect.shape[1] == 4:
            pts_3d_velo = np.hstack([pts_3d_velo, pts_3d_rect[:,3][:,None]])
        return pts_3d_velo

    def project_velo_to_rect(self, pts_3d_velo):
        """ Projects velo to rect

        pts_3d_rect - nx3 or nx4 where 4th channel is intensity
        """
        pts_3d_ref = self.project_velo_to_ref(pts_3d_velo[:,0:3])
        pts_3d_rect = self.project_ref_to_rect(pts_3d_ref)
        if pts_3d_velo.shape[1] == 4:
            pts_3d_rect = np.hstack([pts_3d_rect, pts_3d_velo[:,3][:,None]])
        return pts_3d_rect

    # ===========================
    # ------- 3d to 2d ----------
    # ===========================
    def project_rect_to_image(self, pts_3d_rect):
        """ Projects rect to camera image

        pts_3d_rect - nx3 or nx4 where 4th channel is intensity
        """
        pts_3d_rect_hom = tforms.cart2hom(pts_3d_rect[:,0:3])
        pts_2d = np.dot(pts_3d_rect_hom, np.transpose(self.P)) # nx3
        pts_2d[:,0] /= pts_2d[:,2]
        pts_2d[:,1] /= pts_2d[:,2]
        pts_2d_cam = pts_2d[:,0:2]
        if pts_3d_rect.shape[1] == 4:
            pts_2d_cam = np.hstack([pts_2d_cam, pts_3d_rect[:,3][:,None]])
        return pts_2d_cam

    def project_velo_to_image(self, pts_3d_velo):
        """ Projects velo to camera image

        pts_3d_rect - nx3 or nx4 where 4th channel is intensity
        """
        pts_3d_rect = self.project_velo_to_rect(pts_3d_velo[:,0:3])
        pts_2d_img = self.project_rect_to_image(pts_3d_rect)
        if pts_3d_velo.shape[1] == 4:
            pts_2d_img = np.hstack([pts_2d_img, pts_3d_velo[:,3][:,None]])
        return pts_2d_img

    def project_rect_to_bev(self, pts_3d_rect):
        """ Projects rect to BEV representation

        In rect coordinates, y is "down" so is reduced
        """
        return np.delete(pts_3d_rect, 1, axis=1)

    def project_velo_to_bev(self, pts_3d_velo):
        """ Projects velo to BEV representation

        In velo coordinates, z is "up" so is reduced
        """
        return np.delete(pts_3d_velo, 2, axis=1)

    # ===========================
    # ------- 2d to 3d ----------
    # ===========================
    def project_image_to_rect(self, uv_depth):
        ''' Input: nx3 first two channels are uv, 3rd channel
                   is depth in rect camera coord.
            Output: nx3 points in rect camera coord.
        '''
        n = uv_depth.shape[0]
        x = ((uv_depth[:,0]-self.c_u)*uv_depth[:,2])/self.f_u + self.b_x
        y = ((uv_depth[:,1]-self.c_v)*uv_depth[:,2])/self.f_v + self.b_y
        pts_3d_rect = np.zeros((n,3))
        pts_3d_rect[:,0] = x
        pts_3d_rect[:,1] = y
        pts_3d_rect[:,2] = uv_depth[:,2]
        return pts_3d_rect

    def project_image_to_velo(self, uv_depth):
        pts_3d_rect = self.project_image_to_rect(uv_depth)
        return self.project_rect_to_velo(pts_3d_rect)

    # ===========================
    # ------- 2d to 2d ----------
    # ===========================


class KittiObjectCalibration(Calibration):
    def __init__(self, calib_filepath, from_video=False):
        if from_video:
            calibs = self.read_calib_from_video(calib_filepath)
        else:
            calibs = self.read_calib_file(calib_filepath)

        # Prep data for base initialization
        P = np.reshape(calibs['P2'], [3,4])
        V2C = np.reshape(calibs['Tr_velo_to_cam'], [3,4])
        C2V = tforms.inverse_rigid_trans(V2C)
        R0 = np.reshape(calibs['R0_rect'], [3,3])

        # Call base initialization
        super().__init__(V2C, C2V, R0, P)

        # add the others for possible use later
        self.P0 = calibs['P0']
        self.P1 = calibs['P1']
        self.P2 = calibs['P2']
        self.P3 = calibs['P3']
        self.R0_rect = calibs['R0_rect']
        self.Tr_velo_to_cam = calibs['Tr_velo_to_cam']
        self.Tr_imu_to_velo = calibs['Tr_imu_to_velo']
        self.calib_dict = calibs

    @staticmethod
    def read_calib_file(filepath):
        ''' Read in a calibration file and parse into a dictionary.
        Ref: https://github.com/utiasSTARS/pykitti/blob/master/pykitti/utils.py
        '''
        data = {}
        with open(filepath, 'r') as f:
            for line in f.readlines():
                line = line.rstrip()
                if len(line)==0: continue
                key, value = line.split(':', 1)
                # The only non-float values in these files are dates, which
                # we don't care about anyway
                try:
                    data[key] = np.array([float(x) for x in value.split()])
                except ValueError:
                    pass
        return data

    @staticmethod
    def read_calib_from_video(calib_root_dir):
        ''' Read calibration for camera 2 from video calib files.
            there are calib_cam_to_cam and calib_velo_to_cam under the calib_root_dir
        '''
        raise NotImplementedError
        data = {}
        cam2cam = KittiCalibration.read_calib_file(os.path.join(calib_root_dir, 'calib_cam_to_cam.txt'))
        velo2cam = KittiCalibration.read_calib_file(os.path.join(calib_root_dir, 'calib_velo_to_cam.txt'))
        Tr_velo_to_cam = np.zeros((3,4))
        Tr_velo_to_cam[0:3,0:3] = np.reshape(velo2cam['R'], [3,3])
        Tr_velo_to_cam[:,3] = velo2cam['T']
        data['Tr_velo_to_cam'] = np.reshape(Tr_velo_to_cam, [12])
        data['R0_rect'] = cam2cam['R_rect_00']
        data['P2'] = cam2cam['P_rect_02']
        return data




class DataExtractor:
    """
    Handles data to be able to extract things from it.

    Agnostic to the type of data manager.
    """
    def __init__(self, DataManager):
        self.DataManager = DataManager

    def extract_lidar_from_objects(self, idx_frame, idx_item, region='frustum'):
        """
        idx_item - can be single item or a list
        """
        # Get needed data
        lidar_data = self.DataManager.get_lidar(idx_frame)
        labels = self.DataManager.get_labels(idx_frame)
        calib = self.DataManager.get_calibration(idx_frame)
        # Extract points
        label = labels[idx_item]
        if region == 'frustum':
            point_filter = maskfilters.filter_points_in_frustum(lidar_data, label.box2d, calib)
        elif region == 'bbox':
            point_filter = maskfilters.filter_points_in_object_bbox(lidar_data, label.box3d, calib)
        else:
            raise RuntimeError('Unknown region type for extraction')
        return lidar_data[point_filter, :]

    def extract_lidar_from_occluded_objects(self, idx, params):
        raise NotImplementedError



class Plane3D:
    def __init__(self, plane_coeffs, calib):
        """
        Creates plane equations in the different reference frames

        Assumption: plane_coeffs coming in are in image frame
        """
        self.p_image = plane_coeffs
        self.p_lidar = np.asarray([plane_coeffs[2], -plane_coeffs[0], -plane_coeffs[1], plane_coeffs[3] + 0.08])
