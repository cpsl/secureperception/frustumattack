# @Author: Spencer Hallyburton <spencer>
# @Date:   2021-02-18
# @Filename: my_tforms.py
# @Last modified by:   spencer
# @Last modified time: 2021-03-20

import numpy as np
import math


def align_x_to_vec(vec):
    """
    Generates rotation matrix to align x axis to the specified vector

    Effectively, align along range to target in a range-az-el system
    """
    r2d = np.linalg.norm(vec[0:2])
    if r2d == 0:
        az = 0
    else:
        az = math.acos(vec[0] / r2d)
    el = math.asin(vec[2] / np.linalg.norm(vec))

    # rotate about z for az, then about -y for el
    R1 = rotz(az)
    R2 = roty(-el)
    return R2 @ R1


def cart2hom(pts_3d):
    ''' Input: nx3 points in Cartesian
        Oupput: nx4 points in Homogeneous by appending 1
    '''
    n = pts_3d.shape[0]
    pts_3d_hom = np.hstack((pts_3d, np.ones((n,1))))
    return pts_3d_hom


def get_yaw_from_bev_corners(bev_corners):
    """
    Get yaw angle from bird's eye view 4 corners

    bev_corners: 4x2
    coordinates defined as: x - forward, y - left, z - up

    0 yaw is defined as the short side perpendicular to the y axis in velo
    yaw can't be more than 90 degrees here
    """

    # Center coordinates
    centered = bev_corners - np.mean(bev_corners, axis=0)[:,None].T
    side_1 = np.linalg.norm(centered[0,:] - centered[1,:])
    side_2 = np.linalg.norm(centered[0,:] - centered[2,:])

    # The shorter side is the front/back sides
    if side_1 <= side_2:
        idx_front_to_back = [(0,1), (2,3)]
    else:
        idx_front_to_back = [(0,2), (1,3)]

    # Get the line to the center of the short side
    vec_mid = np.mean(centered[idx_front_to_back[0],:], axis=0)

    # Yaw calculation
    return np.arctan2(vec_mid[0], vec_mid[1])


def rotx(t):
    ''' 3D Rotation about the x-axis. '''
    c = np.cos(t)
    s = np.sin(t)
    return np.array([[1,  0,  0],
                     [0,  c, -s],
                     [0,  s,  c]])


def roty(t):
    ''' Rotation about the y-axis. '''
    c = np.cos(t)
    s = np.sin(t)
    return np.array([[c,  0,  s],
                     [0,  1,  0],
                     [-s, 0,  c]])


def rotz(t):
    ''' Rotation about the z-axis. '''
    c = np.cos(t)
    s = np.sin(t)
    return np.array([[c, -s,  0],
                     [s,  c,  0],
                     [0,  0,  1]])


def transform_from_rot_trans(R, t):
    ''' Transforation matrix from rotation matrix and translation vector. '''
    R = R.reshape(3, 3)
    t = t.reshape(3, 1)
    return np.vstack((np.hstack([R, t]), [0, 0, 0, 1]))


def inverse_rigid_trans(Tr):
    ''' Inverse a rigid body transform matrix (3x4 as [R|t])
        [R'|-R't; 0|1]
    '''
    inv_Tr = np.zeros_like(Tr) # 3x4
    inv_Tr[0:3,0:3] = np.transpose(Tr[0:3,0:3])
    inv_Tr[0:3,3] = np.dot(-np.transpose(Tr[0:3,0:3]), Tr[0:3,3])
    return inv_Tr


def project_to_image(pts_3d, P):
    ''' Project 3d points to image plane.

    Usage: pts_2d = projectToImage(pts_3d, P)
      input: pts_3d: nx3 matrix
             P:      3x4 projection matrix
      output: pts_2d: nx2 matrix

      P(3x4) dot pts_3d_extended(4xn) = projected_pts_2d(3xn)
      => normalize projected_pts_2d(2xn)

      <=> pts_3d_extended(nx4) dot P'(4x3) = projected_pts_2d(nx3)
          => normalize projected_pts_2d(nx2)
    '''
    n = pts_3d.shape[0]
    pts_3d_extend = np.hstack((pts_3d, np.ones((n,1))))
    # print(('pts_3d_extend shape: ', pts_3d_extend.shape))
    pts_2d = np.dot(pts_3d_extend, np.transpose(P)) # nx3
    pts_2d[:,0] /= pts_2d[:,2]
    pts_2d[:,1] /= pts_2d[:,2]
    return pts_2d[:,0:2]


def project_cartesian_to_5_channel_spherical(point_cloud, nchan_h=64, nchan_w=512):
    """
    Takes a point cloud and converts to spherical representation with features

    Features are used from the LU-Net paper and are (x, y, z, intensity, r)
    Standard shape: (64, 512, 5)
    https://arxiv.org/pdf/1710.07368.pdf

    Coordinate system: LiDAR is x forward, y left, z up
    """
    # Project to spherical
    r_3d = np.linalg.norm(point_cloud[:,:3], axis=1)
    r_2d = np.linalg.norm(point_cloud[:,:2], axis=1)
    zen = np.arcsin(point_cloud[:,2] / r_3d)
    az = np.arcsin(point_cloud[:,1] / r_2d)

    # Only include within front-view (define as a 90 degree azimuth pyramid??)
    half_ang_az = math.pi / 180 * 45

    # Define the bin width
    del_w = 2*half_ang_az / nchan_w
    del_h = (max(zen) - min(zen)) / nchan_h  # just do empirically

    # Assign bins
    bin_w = np.floor((az + math.pi/4) / del_w)
    bin_h = np.floor( (zen-min(zen)) / del_h)

    # Build the matrix by looping
    spher_img = np.zeros((nchan_h, nchan_w, 5))
    for ipt in range(point_cloud.shape[0]):
        # To filter by viewing angle, ignore bins outside of desired range
        if (bin_w[ipt] < 0) or (bin_w[ipt] >= nchan_w) or (bin_h[ipt] < 0) or (bin_h[ipt] >= nchan_h):
            continue

        # Flip x and y by convention (?)
        row = nchan_h - int(bin_h[ipt]) - 1
        col = nchan_w - int(bin_w[ipt]) - 1

        # Assign features
        spher_img[row, col, 0:3] = point_cloud[ipt,0:3]
        spher_img[row, col, 3] = point_cloud[ipt,3]
        spher_img[row, col, 4] = r_3d[ipt]

    return spher_img


def project_to_bev():
    raise NotImplementedError
