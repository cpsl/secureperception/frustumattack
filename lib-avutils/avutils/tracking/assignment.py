# @Author: Spencer Hallyburton <spencer>
# @Date:   2021-07-25
# @Filename: assignment.py
# @Last modified by:   spencer
# @Last modified time: 2021-08-13

import numpy as np
from scipy.optimize import linear_sum_assignment


def single_frame_assign(A, algorithm='linear_sum', all_assigned=False):
    """
    Run assignment algorithm

    Rows are considered "detections" and columns are consisdered "tracks"
    (or truths). Thus, any detection not assigned to a track may either be a
    false positive or a new object not yet tracked. Similarly, any track not
    assigned a detection was either a false negative or the object has been
    removed from the frame. For clarity, these are denoted "lone_det" and
    "lone_trk"

    INPUTS
    :A -- cost matrix
    :algorithm -- the name of the assignment algorithm
    :all_assigned -- boolean for if all entries need an assignment or if we can
        allow the algorithm to not assign for high cost
    """

    assert(algorithm in ['hungarian'])

    assignments = []

    if not all_assigned:
        # Eliminate rows that are all 0's ==> lone detections (FP)
        idx_lone_det = [i for i in range(A.shape[0]) if np.all(A[i,:] == np.inf)]
        # Eliminate columns that are all 0's ==> lone tracks (FN)
        idx_lone_trk = [j for j in range(A.shape[1]) if np.all(A[:,j] == np.inf)]
    else:
        idx_lone_det = []
        idx_lone_trk = []

    if algorithm == 'hungarian':
        # replace infinite matrix elements with large number
        large_value = 1e8
        A = np.where(A == np.inf, large_value, A)
        # Run assignment algorithm
        row_ind, col_ind = linear_sum_assignment(A, maximize=False)
    else:
        raise NotImplementedError

    # Any elements not in row_ind are lone detections (false positives)
    idx_lone_det.extend([i for i in range(A.shape[0]) if
                        (i not in row_ind) and (i not in idx_lone_det)])

    if not all_assigned:
        # Add any element that was assigned to a "lone trk"
        idx_lone_det.extend([row_ind[i] for i, id in enumerate(col_ind) if ((id in idx_lone_trk) and (row_ind[i] not in idx_lone_det))])

    # Any elements not in col_ind are lone tracks (false negatives)
    idx_lone_trk.extend([j for j in range(A.shape[1]) if
                        (j not in col_ind) and (j not in idx_lone_trk)])

    if not all_assigned:
        # Add any element that was assigned to a "lone det"
        idx_lone_trk.extend([col_ind[i] for i, id in enumerate(row_ind) if ((id in idx_lone_det) and (col_ind[i] not in idx_lone_trk))])

    # Remove elements from assignment
    assignments = [(ri, cj) for ri, cj in zip(row_ind, col_ind) if
                        (ri not in idx_lone_det) and (cj not in idx_lone_trk)]

    return assignments, sorted(idx_lone_det), sorted(idx_lone_trk)


def multi_frame_assignment():
    raise NotImplementedError
