# @Author: Spencer Hallyburton <spencer>
# @Date:   2021-10-19
# @Filename: datasets.py
# @Last modified by:   spencer
# @Last modified time: 2021-10-19

# Utilities for working with AV datasets
import os
import glob
import shutil
import abc
import numpy as np
from tqdm import tqdm
from cv2 import imread, imwrite

REGISTERED_DATASET_CLASSES = {}

def register_dataset(cls, name=None):
    global REGISTERED_DATASET_CLASSES
    if name is None:
        name = cls.__name__
    assert name not in REGISTERED_DATASET_CLASSES, f"exist class: {REGISTERED_DATASET_CLASSES}"
    REGISTERED_DATASET_CLASSES[name] = cls
    return cls

def get_dataset_class(name):
    global REGISTERED_DATASET_CLASSES
    assert name in REGISTERED_DATASET_CLASSES, f"available class: {REGISTERED_DATASET_CLASSES}"
    return REGISTERED_DATASET_CLASSES[name]


# ===============================================================
# BASE CLASS
# ===============================================================

class Dataset():
    """
    Define a dataset of perception data

    The Dataset classes provide common methods for reading, writing, and
    manipulating perception data. These are not limited to objet detection
    datasets and can be extended to other datasets
    """
    name = ''
    subfolders_essential = []
    subfolders_optional = []
    subfolders_all = []

    def __init__(self, data_dir, split):
        """to instantiate a particular dataset"""
        self.data_dir = data_dir
        self.split = split
        self.split_path = os.path.join(self.data_dir, self.split)
        assert os.path.exists(self.split_path), '{} does not exist yet!!'.format(self.split_path)

    def __len__(self):
        return len(self.idxs)

    @classmethod
    def make_new(cls, data_dir, split):
        A = super().__new__(cls)
        A.__init__(data_dir, split)
        return A

    @property
    def idxs(self):
        """Wrapper to the classmethod using the split path"""
        return self._get_idxs_folder(self.split_path)

    def check_idx(self, idx):
        assert idx in self.idxs, 'Candidate index, %i, not in index set' % (idx)

    @abc.abstractmethod
    def get_data(self, idx, folder=None):
        """Returns a dictionary with data for a frame"""

    @abc.abstractmethod
    def wipe_experiment(cls, path_to_folder, remove_tree=False):
        """Delete an experiment"""

    @abc.abstractmethod
    def copy_experiment_from_to(cls, src_base_path, dest_base_path, nframes=None, frame_list=None):
        """Copies an experiment from a source to dest folder"""

    @abc.abstractmethod
    def idx_to_prefix(idx):
        """Convert an idx (unique file ID) to a string"""

    @abc.abstractmethod
    def _get_idxs(path_to_folder):
        """Get the indices within a data folder"""

    @classmethod
    def _get_idxs_folder(cls, folder):
        """Wrapper around get indices using sets"""
        idxs = None
        for subfolder in cls.subfolders_essential:
            full_path = os.path.join(folder, subfolder)
            if idxs is None:
                idxs = set(cls._get_idxs(full_path))
            else:
                idxs = idxs.intersection(set(cls._get_idxs(full_path)))
        return np.asarray(sorted(list(idxs)))

    @classmethod
    def _get_frame_list_from_nframes(cls, folder_path, nframes):
        all_idxs = cls._get_idxs_folder(folder_path)
        if nframes > len(all_idxs):
            print('Requested nframes ({}) is larger than number of frames ({}), so taking all'.format(nframes, len(all_idxs)))
            nframes = len(all_idxs)
        return all_idxs[:nframes]

    @classmethod
    def _wipe_data(cls, path_to_folder):
        """Delete specifically the data of an experiment"""
        cls._check_folder_before_bad_things(path_to_folder)
        if os.path.exists(path_to_folder):
            if os.path.islink(path_to_folder):
                os.unlink(path_to_folder)
            else:
                shutil.rmtree(path_to_folder)

    @classmethod
    def _copy_data_from_to(cls, src_base_path, dest_base_path, frame_list):
        """Copy specifically the data from an experiment"""
        # Get frame list
        all_idxs = cls._get_idxs_folder(src_base_path)
        if np.all(frame_list == all_idxs):
            copy_all = True  # TODO: JUST MAKE THIS A SYMLINK INSTEAD (?)
        else:
            copy_all = False

        # Copy files included in the frame list to new destimation
        print('Copying each of %i subfolders' % len(cls.subfolders_all))
        for subfolder in tqdm(cls.subfolders_all):
            src_folder = os.path.join(src_base_path, subfolder)
            dest_folder = os.path.join(dest_base_path, subfolder)

            # Check if source exists
            if not os.path.exists(src_folder):
                continue

            # If copy all, use shutil, otherwise, copy one by one
            if os.path.exists(dest_folder):
                if os.path.islink(dest_folder):
                    os.unlink(dest_folder)
                else:
                    shutil.rmtree(dest_folder)
            if copy_all:
                shutil.copytree(src_folder, dest_folder)
            else:
                os.makedirs(dest_folder, exist_ok=True)
                for idx_f in frame_list:
                    prefix = cls.idx_to_prefix(idx_f)
                    fname = glob.glob(os.path.join(src_folder, '{}.*'.format(prefix)))

                    assert len(fname) == 1, 'Only one frame can match the glob'
                    shutil.copy(fname[0], fname[0].replace(src_folder, dest_folder))

    @staticmethod
    def _check_folder_before_bad_things(path_to_folder):
        """Ensure that we specially mark folders before we do things like delete"""
        acceptable_markers = ['experiment', 'TEST', 'temp']
        found_mark = False
        for mark in acceptable_markers:
            if mark in path_to_folder:
                found_mark = True
        if not found_mark:
            raise RuntimeError('Must have an acceptable marker word in folder name to proceed')


# ===============================================================
# OBJECT DATASETS
# ===============================================================
class Object3dDataset(Dataset):

    def get_data(self, idx):
        data = {}
        folder_names_inv = {v:k for k, v in self.folder_names.items()}
        for subfolder in self.subfolders_all:
            if os.path.exists(os.path.join(self.split_path, subfolder)):
                fname_inv = folder_names_inv[subfolder]
                data[fname_inv] = self.get_data_by_type(idx, fname_inv)
        return data

    def get_data_by_type(self, idx, data_string):
        if data_string.lower() in ['label']:
            return self.get_labels(idx)
        elif data_string.lower() in ['image']:
            return self.get_image(idx)
        elif data_string.lower() in ['lidar']:
            return self.get_lidar(idx)
        elif data_string.lower() in ['calibration']:
            return self.get_calibration(idx)
        else:
            raise NotImplementedError('Cannot return {} type'.format(data_string))

    def get_calibration(self, idx):
        raise NotImplementedError

    def get_image(self, idx):
        raise NotImplementedError

    @abc.abstractmethod
    def get_labels(self, idx):
        """This must be implemented"""

    def get_lidar(self, idx):
        raise NotImplementedError

    def view_data(self, idx, idx_label=None, lidar_extent=None, addbox=[], colormethod='depth'):
        image_good = ('image' in self.folder_names) and (os.path.exists(os.path.join(self.split_path, self.folder_names['image'])))
        lidar_good = ('lidar' in self.folder_names) and (os.path.exists(os.path.join(self.split_path, self.folder_names['lidar'])))
        if image_good:
            self.view_camera_data(idx, idx_label, addbox)
        else:
            print('Cannot show image with labels')
        if image_good and lidar_good:
            self.view_lidar_on_camera(idx, idx_label, colormethod)
        else:
            print('Cannot show lidar on image')
        if lidar_good:
            self.view_lidar_bev(idx, idx_label, lidar_extent, colormethod)
        else:
            print('Cannot show lidar bev')

    def view_camera_data(self, idx, idx_label=None, addbox=[]):
        from avutils.perception import visualize
        calib = self.get_calibration(idx)
        img = self.get_image(idx)
        labels = self.get_labels(idx)
        if idx_label is not None:
            labels = labels[idx_label]
        visualize.show_image_with_boxes(img, labels, calib, show3d=True, inline=True, addbox=addbox)

    def view_lidar_on_camera(self, idx, idx_label, colormethod='depth'):
        from avutils.perception import visualize
        img = self.get_image(idx)
        calib = self.get_calibration(idx)
        labels = self.get_labels(idx)
        if idx_label is not None:
            labels = labels[idx_label]
        lidar = self.get_lidar(idx)
        visualize.show_lidar_on_image(lidar, img, calib, inline=True, colormethod=colormethod)

    def view_lidar_bev(self, idx, idx_label, extent=None, colormethod='depth'):
        from avutils.perception import visualize
        calib = self.get_calibration(idx)
        labels = self.get_labels(idx)
        if idx_label is not None:
            labels = labels[idx_label]
        lidar = self.get_lidar(idx)
        visualize.show_lidar_bev(lidar, extent=extent, calib=calib, labels=labels, colormethod=colormethod)


# ===============================================================
# SPECIFIC DATASETS
# ===============================================================

@register_dataset
class KittiObjectDataset(Object3dDataset):
    from avutils.perception.types import KittiObjectCalibration
    from avutils.perception.types import KittiObject3DLabel
    from avutils.perception.types import Plane3D

    name = 'KITTI'
    subfolders_essential = ['velodyne', 'image_2', 'calib', 'label_2']
    subfolders_optional = ['planes', 'velodyne_CROP', 'velodyne_reduced']
    subfolders_all = list(set(subfolders_essential).union(set(subfolders_optional)))
    folder_names  = {'image':'image_2', 'lidar':'velodyne', 'label':'label_2', 'calibration':'calib', 'ground':'planes', 'disparity':'disparity'}

    CFG = {}
    CFG['num_lidar_features'] = 4
    CFG['IMAGE_WIDTH'] = 1242
    CFG['IMAGE_HEIGHT'] = 375
    CFG['IMAGE_CHANNEL'] = 3

    def __init__(self,  data_dir, split):
        super().__init__(data_dir, split)

    @classmethod
    def wipe_experiment(cls, path_to_folder, remove_tree=False):
        imset_path = cls._get_imset_path_from_data_path(path_to_folder)
        cls._wipe_imageset(imset_path)
        cls._wipe_data(path_to_folder)

    @classmethod
    def copy_experiment_from_to(cls, src_base_path, dest_base_path, nframes=None, frame_list=None):
        # Get frame list
        check_xor_for_none(nframes, frame_list)
        if frame_list is None:
            frame_list = cls._get_frame_list_from_nframes(src_base_path, nframes)

        # Run data copying
        cls._copy_data_from_to(src_base_path, dest_base_path, frame_list)
        imset_dest = cls._get_imset_path_from_data_path(dest_base_path)
        cls._write_imset(imset_dest, frame_list)

    def get_calibration(self, idx):
        self.check_idx(idx)
        calib_fname = os.path.join(self.split_path, self.folder_names['calibration'], '%06d.txt'%idx)
        return self.KittiObjectCalibration(calib_fname)

    @classmethod
    def save_calibration(cls, calib, path, idx):
        calib_filename = os.path.join(path, '%06d.txt'%idx)
        if not os.path.isdir(path):
            os.makedirs(path, exist_ok=True)
        with open(calib_filename, 'w') as txt_file:
            for key, values in calib.calib_dict.items():
                txt_file.write('{}:'.format(key))
                for val in values:
                        txt_file.write(' {}'.format(val))
                txt_file.write('\n')

    def get_disparity(self, idx, is_depth=True):
        self.check_idx(idx)
        disp_fname = os.path.join(self.split_path, self.folder_names['disparity'], '%06d.{}')
        if is_depth:
            return np.squeeze(np.load(disp_fname.format('npy')))
        else:
            return imread(disp_fname.format('jpeg'))

    def get_ground(self, idx):
        """Returns the coefficients of the ground plane in the camera frame"""
        self.check_idx(idx)
        ground_fname = os.path.join(self.split_path, self.folder_names['ground'], '%06d.txt'%(idx))
        coeffs_string = [line.rstrip() for line in open(ground_fname)][3]
        ground_plane = np.asarray([float(st) for st in coeffs_string.split()])
        if ground_plane[1] > 0:
            ground_plane = -ground_plane
        ground_plane = ground_plane / np.linalg.norm(ground_plane[0:3])
        return self.Plane3D(ground_plane, self.get_calibration(idx))

    @classmethod
    def save_ground(cls, ground, path, idx):
        ground_filename = os.path.join(path, '%06d.txt'%idx)
        if not os.path.isdir(path):
            os.makedirs(path, exist_ok=True)
        ground_write = '# Plane\nWidth 4\nHeight 1\n'
        coeffstr = ''
        for i in range(len(ground)):
            coeffstr += ' %.6e'%ground[i]
        ground_write += coeffstr[1::]  # remove leading space
        with open(ground_filename, 'w') as txt_file:
            txt_file.write(ground_write)

    def get_image(self, idx):
        self.check_idx(idx)
        img_fname = os.path.join(self.split_path, self.folder_names['image'], '%06d.png'%idx)
        assert os.path.exists(img_fname), f'Cannot find requested image file at {img_fname}'
        return imread(img_fname)

    @classmethod
    def save_image(cls, image, path, idx):
        image_filename = os.path.join(path, '%06d.png'%idx)
        if not os.path.isdir(path):
            os.makedirs(path, exist_ok=True)
        imwrite(image_filename, image)

    def get_labels(self, idx, whitelist_types=['Car', 'Pedestrian', 'Cyclist'], ignore_types=['DontCare']):
        self.check_idx(idx)
        label_file = os.path.join(self.split_path, self.folder_names['label'], '%06d.txt'%idx)
        return self.get_labels_from_file(label_file, whitelist_types, ignore_types)

    @classmethod
    def save_labels(cls, labels, path, idx, add_label_folder):
        if add_label_folder:
            label_folder_path = os.path.join(path, self.data_names['label'])
        else:
            label_folder_path = path
        cls._save_labels(labels, label_folder_path, idx)

    @classmethod
    def _save_labels(cls, labels, path, idx):
        label_txt_list = [lab.write_text() for lab in labels]
        cls.write_label_text(label_txt_list, path, idx)

    @staticmethod
    def write_label_text(label_txt_list, path, idx):
        if not os.path.isdir(path):
            os.makedirs(path, exist_ok=True)
        fname = os.path.join(path, '%06d.txt'%idx)
        with open(fname, 'w') as f:
            first = True
            for ltxt in label_txt_list:
                if not first:
                    ltxt = '\n' + ltxt
                else:
                    first = False
                f.write(ltxt)

    @classmethod
    def get_labels_from_file(cls, label_fname, whitelist_types=['Car', 'Pedestrian', 'Cyclist'], ignore_types=['DontCare']):
        assert os.path.exists(label_fname), f'Cannot find requested label file at {label_fname}'
        with open(label_fname, 'r') as f:
            lines = [line.rstrip() for line in f]
        labels = [cls.KittiObject3DLabel(line) for line in lines]
        if whitelist_types == 'all':
            labs = labels
        else:
            labs = [lab for lab in labels if (lab.obj_type not in ignore_types) & (lab.obj_type in whitelist_types)]
        labs = np.asarray(labs)
        return labs

    @classmethod
    def get_labels_from_directory(cls, dir, idxs=None):
        """Get all labels from a directory of text files"""
        # Loop over files
        directory = os.fsencode(dir)
        if len(os.listdir(directory)) == 0:
            print('No labels to load')
            return [], []
        idx_all = []
        labels_all = []
        for file in os.listdir(directory):
            filename = os.fsdecode(file)
            if filename.endswith(".txt"):
                if 'log' in filename:
                    continue
                idx = int(filename.strip('.txt'))
                if idxs is not None:
                    # Check for index match
                    try:
                        iterator = iter(idxs)
                    except TypeError:
                        # not iterable
                        if not idx == idxs:
                            continue
                    else:
                        # iterable
                        if idx not in idxs:
                            continue
                label_fname = os.path.join(dir, '%06d.txt'%idx)
                labels = cls.get_labels_from_file(label_fname)
                idx_all.append(idx)
                labels_all.append(labels)
        idx_all, labels_all = (list(t) for t in zip(*sorted(zip(idx_all, labels_all))))
        return labels_all, idx_all

    @staticmethod
    def save_labels_to_folder(label_folder_path, labels, idx):
        if type(labels) != np.ndarray:
            labels = np.asarray(labels)
        if labels.size == 0:
            return
        label_txt_list = [lab.write_text() for lab in labels]
        if not os.path.isdir(label_folder_path):
            os.makedirs(label_folder_path, exist_ok=True)
        label_filename = os.path.join(label_folder_path, '%06d.txt'%(idx))
        with open(label_filename, 'w') as f:
            first = True
            for ltxt in label_txt_list:
                if not first:
                    ltxt = '\n' + ltxt
                f.write(ltxt)
                first = False

    def get_lidar(self, idx):
        self.check_idx(idx)
        lidar_fname = os.path.join(self.split_path, self.folder_names['lidar'], '%06d.bin'%idx)
        lidar = np.fromfile(lidar_fname, dtype=np.float32).reshape((-1, self.CFG['num_lidar_features']))
        return lidar[lidar[:,0]>0,:]

    @classmethod
    def save_lidar(cls, lidar, path, idx):
        lidar_filename = os.path.join(path, '%06d.bin'%idx)
        if not os.path.isdir(path):
            os.makedirs(path, exist_ok=True)
        lidar.tofile(lidar_filename)

    @classmethod
    def _wipe_imageset(cls, imset_path):
        cls._check_folder_before_bad_things(imset_path)
        if os.path.exists(imset_path):
            print('removing: %s' % imset_path)
            os.remove(imset_path)

    @staticmethod
    def _write_imset(imset_path, frame_list):
        with open(imset_path, 'w') as f:
            for idx in frame_list:
                f.write('%06d\n' % int(idx))

    @staticmethod
    def idx_to_prefix(idx):
        return '%06d' % idx

    @staticmethod
    def _get_idxs(path_to_folder):
        """Gets indices of items in a folder by KITTI standard"""
        fnames = glob.glob(os.path.join(path_to_folder, '*.*'))
        return sorted([int(f.strip().split('/')[-1].split('.')[-2]) for f in fnames])

    @staticmethod
    def _get_imset_path_from_data_path(path_to_folder):
        exp_name = path_to_folder.split('/')[-1]
        imset_folder = os.path.join(*path_to_folder.split('/')[:-1], 'ImageSets')
        if path_to_folder[0] == '/':
            imset_folder = '/' + imset_folder
        assert os.path.exists(imset_folder), f'ImageSets folder must exist 1 level above the data (tried {imset_folder})'
        return os.path.join(imset_folder, exp_name +'.txt')


# ===============================================================
# UTILITIES
# ===============================================================

def check_xor_for_none(a, b):
    assert check_xor(a is None, b is None), "Can only pass in one of these inputs"

def check_xor(a, b):
    return (a or b) and (not a or not b)

def remove_glob(glob_files):
    rem = False
    for f in glob_files:
        os.remove(f)
        rem = True
    if rem:
        print('Removed files from: {}'.format(os.path.dirname(f)), flush=True)
