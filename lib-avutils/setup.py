# @Author: Spencer Hallyburton <spencer>
# @Date:   2021-07-13
# @Filename: setup.py
# @Last modified by:   spencer
# @Last modified time: 2021-07-13


from setuptools import setup, find_packages

setup(name='avutils', version='1.0', packages=find_packages())
