# Frustum Attack

The frustum attack is a limited-knowledge attack against modern perception algorithms built on deep neural networks. The frustum attack builds off of prior naive spoofing attacks by demonstrating an ability to compromise camera + LiDAR sensor fusion, as well as LiDAR-only perception.

Please see [https://arxiv.org/pdf/2106.07098.pdf](https://arxiv.org/pdf/2106.07098.pdf) for full details on the frustum attack.


See how just adding a few points behind real objects can cause the perception algorithm to detect the spoofed points as a false positive object. This is because, by spoofing in the frustum, the spoof points maintain both consistency with the image front-view and consistency with physical invariants, such as the shadow region behind normal objects.
![Frustum attack in action against Frustum PointNet](frustum_attack.png)

## Organization
This repository is organized as follows. The top level contains modules (avutils, percep attacks, pyutils) that must be installed. The top level also contains perception algorithms, all of which are open-source and can be access online. We have included two perception algorithms in this repo to demo how to run the frustum attack. Finally, we have included a demo script which shows how to run the frustum attack and naive spoofing.

## Installation

### Install Modules

As of now, submodules have been removed from the project to facilitate easier startup of the project but at the cost of larger repo size.

To start, clone the repository as usual

```
git clone https://gitlab.oit.duke.edu/cpsl/secureperception/frustumattack.git
```

Then, create an environment, preferably with conda, as
```
conda create -n frustum-attack python jupyter matplotlib tqdm psutil pandas seaborn opencv -c menpo
```

Then, install the custom modules as packages with
```
pip install -e lib-avutils -e lib-percep_attacks -e lib-pyutils
```


### Set up Algorithms
Algorithms may need to be set up (e.g. ops compiled and installed for your machine). Refer to the original repositories for these instructions. We have modified the algorithms only such that they can be commanded under our test infrastructure. Additional algorithms can also be set up at the users discretion.

The algorithms will likely require a symbolic link to your KITTI dataset location. See each of the algorithms for instructions.

The redistribution of these algorithms is allowed, as we maintain the original licenses in each subfolder. Please see the algorithms subfolders for details on the licenses of each of the algorithms. Modifications to algorithms are only made to the I/O infrastructure (loading, saving, shell script to command run the algorithms) and no modifications were made to the underlying algorithms or models themselves. 

### Demo Script
Fire up a jupyter notebook and open up DemoSpoofingAttacks.ipynb


### Citing Our Work
```
@article{hallyburton2021security,
  title={Security Analysis of Camera-LiDAR Fusion Against Black-Box Attacks on Autonomous Vehicles},
  author={Hallyburton, R Spencer and Liu, Yupei and Cao, Yulong and Mao, Z Morley and Pajic, Miroslav},
  journal={arXiv preprint arXiv:2106.07098},
  year={2021}
}
```
